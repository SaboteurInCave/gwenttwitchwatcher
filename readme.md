# Gwent Twitch Watcher

Simple bot for Discord servers. 

This little creature watches for twitch Gwent streams, posts information about it into the selected room and removes message after stream closing.


## How to install 

1. Obtain a discord bot token (https://discordapp.com/developers/) and register bot on your discord server
2. Obtain a twitch api key (https://glass.twitch.tv/console/ - for endpoint you can use ``http://localhost``)
3. Install a mongodb.
4. Configure the bot by using the _bot/.env_ and other config files in `./config/` folder.
4. Run the bot by typing the following command ``npm run start``

## Available commands

1. ``!start_stream_watching`` - start the watching process
2. ``!stop_stream_wathcing`` - stop the watching process

## Permissions

You need to give bot the permission to remove messages in the selected room + give roles

## Dockerfile

    FROM node:latest
    
    COPY ./bot/package.json /data/
    WORKDIR /data/
    
    RUN npm install
    ENV PATH /data/node_modules/.bin:$PATH
    
    COPY bot/ /data/app/
    WORKDIR /data/app/
    
    CMD ["npm", "run", "start"]


## docker-compose.yml

    version: '3'

    services:
      bot:
        container_name: <your_name>
        build: .
        links:
        - mongo
        volumes:
        - ./bot:/data/app
    
      mongo:
        container_name: <your_name>
        image: mongo
        volumes:
        - ./data:/data/db
    
      redis:
        container_name: <your_name>
        image: redis


## bot/.env file structure

        NODE_ENV=production # if you are on production, 'development' otherwise
        DISCORD_TOKEN=<dicord_bot_token here>
        TWITCH_TOKEN=<twitch_access_token here>
        
        MONDO_ADDRESS=<address> # localhost or docker service name
        MONGO_PORT=27017 # or another port
        MONGO_DATABASE=<database_name>
        
        REDIS_ADDRESS=<address> # localhost or another host
        REDIS_PORT=6379 # or another port
        REDIS_PASSWORD=<password> # here password
        
        # need if you are in the production mode
        MONGO_INITDB_ROOT_USERNAME=<root>
        MONGO_INITDB_ROOT_PASSWORD=<strong_root_pass>

## bot/basic.config.js file structure
    module.exports = {
        // commands will start with
        prefix: '!',
        // bot name for all embed messages
        bot_name: 'GwentTwitchWatcher', // or another bot name
        // users from groups (+ admin) are allowed to interact with bot
        groups: ['Moderators'],
        
        // removing rate of messages
	    removeRate: 30000,
    
        // internal discord token
        discord_token: process.env.DISCORD_TOKEN,
        // internal storage for watchers
        watchers: {} // DO NOT TOUCH IT
    };

In case if you want to override this file, just create a `<someName>.config.js` with same structure and new settings.

## watcher setting

You also can specify your settings for watchers (Twitch and Gwent watchers) in `./config/<name>.watcher.js`

### Twitch.watcher.js

        const path = require('path');


        module.exports = {
            // | watcher options | \\

            // access token
            twitch_token: process.env.TWITCH_TOKEN,
            // update rate (ms)
            rate: 30000,
            // allowed languages for watching, if you want all languages - just leave empty array
            languages: ['ru'],
            // allowed channel for posting - set id of channel
            channel: '544148683340382249',
        
            // switch bot into the strict mode - only mentioned in the allowed_streamers channels will be displayed
            strict_mode: false,
            allowed_streamers: [],
        
        
            // | queue options | \\
        
            // queue name (need to be the same as config/<queue_name>.watcher.js)
            queue_name: 'twitch',
            // path to watcher module
            path: path.resolve(path.normalize(__dirname + '/..'), 'queue', 'twitchWatcher.js'),
        };

### Gwent.watcher.js

        const path = require('path');
        
        module.exports = {
            rate: 15000, // update rate (ms)

        
            queue_name: 'gwent',
            // path to watcher module
            path: path.resolve(path.normalize(__dirname + '/..'), 'queue', 'gwentWatcher.js'),
        };

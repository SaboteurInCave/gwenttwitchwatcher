import { factionsEnum } from './enums';
import moment from 'moment';
import 'moment-timezone';
import logger from './logger';

const numberRegex = new RegExp(/\d+/g);

// embed

export const addGogNickname = (discord, gog) => {
	if (gog !== discord) {
		return ` (GOG: ${gog})`;
	}
	else {
		return '';
	}
};

export const formateDate = (date) => {
	return moment(date).tz('Europe/Moscow').format('DD-MM-YYYY HH:mm');
};

// time measurement

export const timestampHoursDifference = (d1, d2) => {
	let diff = d2 > d1 ? d2 - d1 : d1 - d2;
	return diff / 3600000.0;
};

// factions

export const getFactions = () => {
	const { NT, NONE, ...factions } = factionsEnum;
	return factions;
};

// site parsing

export const removeSpaces = (data) => {
	return data.replace(/\s/g, '');
};

export const parseWinsStats = (data) => {
	const mapping = {
		scoiatael: factionsEnum.SC,
		monsters: factionsEnum.MO,
		northernrealms: factionsEnum.NR,
		skellige: factionsEnum.SK,
		nilfgaard: factionsEnum.NG,
		syndicate: factionsEnum.SY,
	};

	const result = {};

	result.overall = data.overall;

	for (const faction of data.factions) {
		result[mapping[faction.slug]] = faction.count;
	}

	return result;
};

export const parseUserProfile = (dom) => {
	const prestige = dom('.l-player-details__prestige').attr('class').split(' ')[1];

	const position = Number(removeSpaces(dom('.l-player-details__table-position').text()).match(numberRegex));
	const mmr = Number(removeSpaces(dom('.l-player-details__table-mmr').text()).match(numberRegex));

	const result = {
		name: removeSpaces(dom('.l-player-details__name').text()),
		level: Number(removeSpaces(dom('.l-player-details__prestige').text())),
		prestige: Number(prestige ? prestige.match(numberRegex)[0] : 0),
		rank: Number(removeSpaces(dom('.l-player-details__rank').text())) || 0,
		mode: dom('.l-player-details__table-ladder').text(),
		position: position,
		mmr: mmr,
	};

	const image = dom('.l-player-details__avatar img').each((index, element) => {
		let field = '';
		if (index === 0) {
			field = 'avatar';
		}
		else if (index === 1) {
			field = 'border';
		}

		if (field !== '') {
			result[field] = Number(dom(element).attr('src') != null && dom(element).attr('src').match(numberRegex) != null && dom(element).attr('src').match(numberRegex).length > 2 ? dom(element).attr('src').match(numberRegex)[2] : 0);
		}
	});

	return result;
};

export const parseSeasonStats = (data) => {
	const season = {
		overall: { mmr: 0, games: 0 },
		games: { wins: 0, loses: 0, ties: 0, winrate: 0 },
		factions: {
			[factionsEnum.NR]: { mmr: 0, games: 0 },
			[factionsEnum.NG]: { mmr: 0, games: 0 },
			[factionsEnum.SC]: { mmr: 0, games: 0 },
			[factionsEnum.MO]: { mmr: 0, games: 0 },
			[factionsEnum.SK]: { mmr: 0, games: 0 },
			[factionsEnum.SY]: { mmr: 0, games: 0 },
		},
	};

	if (data.length > 0) {
		const overall = removeSpaces(data[1]).match(numberRegex).map(num => Number(num));
		season.overall.mmr = overall[0];
		season.overall.games = overall[1];

		const results = (removeSpaces(data[3]) + removeSpaces(data[5]) + removeSpaces(data[7])).match(numberRegex).map(num => Number(num));
		season.games.wins = results[0];
		season.games.loses = results[1];
		season.games.ties = results[2];
		season.games.winrate = season.games.wins / season.overall.games;

		if (isNaN(season.games.winrate)) {
			season.games.winrate = 0;
		}

		/*
			2019-04-09 11:39:01 debug: '0fMMR0матчей'
			2019-04-09 11:39:01 debug: 'Нильфгаард'
			2019-04-09 11:39:01 debug: '675fMMR11матчей'
			2019-04-09 11:39:01 debug: 'КоролевстваСевера'
			2019-04-09 11:39:01 debug: '0fMMR0матчей'
			2019-04-09 11:39:01 debug: 'Чудовища'
			2019-04-09 11:39:01 debug: '0fMMR0матчей'
			2019-04-09 11:39:01 debug: 'Скеллиге'
			2019-04-09 11:39:01 debug: '0fMMR0матчей'
			2019-04-09 11:39:01 debug: 'Скоя’таэли'
		 */

		// 1 - total (mmr, games)
		// 3 - wins
		// 5 - loses
		// 7 - ties
		// 9 - nilf (mmr, games)
		// 11 - scoa (mmr, games)
		// 13 - northern realms (mmr, games)
		// 15 - skillige (mmr, games)
		// 17 - monsters (mmr, games)

		// todo: make localizaton - throw identifiers to config file!

		const factions = {
			'Нильфгаард': factionsEnum.NG,
			'КоролевстваСевера': factionsEnum.NR,
			'Чудовища': factionsEnum.MO,
			'Скеллиге': factionsEnum.SK,
			'Скоя’таэли': factionsEnum.SC,
			'Синдикат': factionsEnum.SY,
		};

		const factionRows = [9, 11, 13, 15, 17, 19];

		for (const row of factionRows) {
			const faction = factions[removeSpaces(data[row - 1])]; // previous row has localized faction name
			const factionResults = removeSpaces(data[row]).match(numberRegex).map(num => Number(num));
			// season.factions[faction].mmr = factionResults[0];
			season.factions[faction].games = factionResults[0];
		}
	}


	return season;

};

import DiscordWatcher from '../utils/discord';
import logger from '../logger';

const addUser = async (guild, emojiUsers, tournamentMembers, group) => {
	for (const user of emojiUsers) {
		if (!tournamentMembers.find(u => u.user.id === user.id)) {
			// need to add role
			const member = guild.member(user);
			if (member) {
				await guild.member(user).roles.add(group); // job.data.tournamentGroup
			}
			else {
				// logger.error(`User ${user.username} is not found as member!`)
			}

		}
	}
};

const removeUser = async (emojiUsers, tournamentMembers, group) => {
	for (const member of tournamentMembers) {
		// logger.info(`tournament user ${member.user.username}`);
		if (emojiUsers.length === 0 || !emojiUsers.find(u => u.id === member.user.id)) {
			// need to remove role
			await member.roles.remove(group);
		}
	}
};

const fetchAllReactionUsers = async (reaction) => {
	const result = [];
	let lastId;

	while (true) {
		const options = { limit: 100 };
		if (lastId) {
			options.after = lastId;
		}

		const users = await reaction.users.fetch(options);

		// all users was fetched
		if (users.size === 0) {
			break;
		}

		result.push(...users.array());
		lastId = users.last().id;
	}

	return result;
};

module.exports = async (job) => {
	try {
		// logger.info('tournament role fetching');
		// logger.info(JSON.stringify(job));

		await DiscordWatcher.connect(job.data.discord_token);
		const channel = await DiscordWatcher.client.channels.fetch(job.data.channel);

		if (channel) {
			// logger.info(`channel ${channel.id}`);
			// logger.info(`required message: ${job.data.message}`);
			const message = await channel.messages.fetch(job.data.message);

			if (message) {
				// logger.info(`message: ${message.content}`);
				const guild = DiscordWatcher.client.guilds.cache.first(); //.members.fetch(); // TODO: it is not good to fetch first guild, but we work only with one server, so ...

				if (guild && guild.available) {
					// logger.info('guild was founded');
					const tournamentMembers = (await guild.roles.fetch(job.data.tournamentGroup)).members.array();
					// logger.info(`tournamentMembers length: ${tournamentMembers.length}`);
					// const tournamentReaction = message.reactions.cache.find(reaction => reaction.emoji.name === '➕'); // for local only
					const tournamentReaction = message.reactions.cache.find(reaction => reaction.emoji.name === job.data.reaction);

					if (tournamentReaction) {
						const emojiUsers = await fetchAllReactionUsers(tournamentReaction); // (await tournamentReaction.users.fetch()).array();
						// logger.info(`emotions user length: ${emojiUsers.length}`);
						await addUser(guild, emojiUsers, tournamentMembers, job.data.tournamentGroup);
						await removeUser(emojiUsers, tournamentMembers, job.data.tournamentGroup);
					}
					else {
						await removeUser([], tournamentMembers, job.data.tournamentGroup);
					}
				}
			}
			else {
				logger.error('Tournament role message not found!');
			}
		}
		else {
			logger.error('Tournament channel not found!');
		}
	} catch (e) {
		logger.error(JSON.stringify(e));
	}
};


process.on('unhandledRejection', error => {
	logger.error('Unhandled promise rejection:', JSON.stringify(error));
});

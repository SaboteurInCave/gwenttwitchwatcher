import Database from '../utils/database';
import logger from '../logger';
import axios from 'axios';
import { getFactions, parseSeasonStats, parseUserProfile, parseWinsStats } from '../utils';
import Stats from '../db/stats';
import User from '../db/user';


import axiosRetry from 'axios-retry';
import { factionsEnum, profileMode } from '../enums';

import DiscordWatcher from '../utils/discord';
import { getTopPlayerRole, removeCustomRoles } from '../utils/roles';
import { getLatestTimestamp, getLeaderboards } from '../utils/functionality';

import moment from 'moment';
import 'moment-timezone';
import { getMinFactionRoleLimit, getTopPlayerAmount, getTopPlayerLimit } from '../utils/settings';

const db = new Database();

const cheerio = require('cheerio');

axiosRetry(axios, { retries: 5, retryDelay: axiosRetry.exponentialDelay });

const getUsernameLog = (user) => {
	return user.gog ? `${user.username} (${user.gog}):` : `${user.username}:`;
};

const processTopPlayers = async (discordMember, userMMR, mmrList, topPlayerLimit) => {
	const topPlayerRole = getTopPlayerRole();

	if (mmrList.length === 0) {
		if (discordMember.roles.cache.has(topPlayerRole)) {
			await discordMember.roles.remove(topPlayerRole, `Не найдено пользователей с MMR >= ${topPlayerLimit}`);
		}
	}
	else {
		const inRange = userMMR >= mmrList[mmrList.length - 1] && userMMR <= mmrList[0];

		if (inRange && !discordMember.roles.cache.has(topPlayerRole)) {
			await discordMember.roles.add(topPlayerRole, `$MMR пользователя - ${userMMR}, доступный диапазон - ${mmrList}`);
		}

		if (!inRange && discordMember.roles.cache.has(topPlayerRole)) {
			await discordMember.roles.remove(topPlayerRole, `$MMR пользователя - ${userMMR}, доступный диапазон - ${mmrList}`);
		}
	}
};

const parseAllPlayers = async (collection, timestamp) => {
	const requests = collection.map(req => {
		const config = {
			headers: {
				'Content-Length': 0,
				'Content-Type': 'text/plain',
			},
			responseType: 'text',
		};

		const username = req.gog === '' ? req.username : req.gog;

		return axios.get(`https://www.playgwent.com/ru/profile/${encodeURI(username)}`, config)
		//wrap all responses into objects and always resolve
			.then(
				(response) => {
					const dom = cheerio.load(response.data);
					const text = dom.text();

					if (text.includes('ПРОФИЛЬ ИГРОКА СКРЫТ')) {
						return {
							id: req.id,
							isHidden: true,
						};
					}
					else {
						const profileWinsRegex = new RegExp('profileDataWins = (.*?);\n');
						const profileWinsCurrentRegex = new RegExp('profileDataCurrent = (.*?);\n');
						const seasonRatingWins = dom('.current-ranked > tbody > tr > td').toArray().map(item => dom(item).text());

						const totalGames = JSON.parse(profileWinsRegex.exec(response.data)[1]);
						const seasonGames = JSON.parse(profileWinsCurrentRegex.exec(response.data)[1]);

						const data = {
							player: req.id,
							profile: parseUserProfile(dom),
							wins: {
								total: parseWinsStats(totalGames),
								season: parseWinsStats(seasonGames),
							},
							season: parseSeasonStats(seasonRatingWins),
							timestamp: timestamp,
						};

						const result = {
							data,
							id: req.id,
							isHidden: false,
						};

						if (req.gog === '') {
							result['newUser'] = {
								id: req.id, gog: req.username,
							};
						}

						return result;
					}


				},
				(err) => {
					return {
						id: req.id,
						isHidden: false,
						error: err.response.status,
					};
				},
			);
	});

	return axios.all(requests)
		.then(axios.spread((...args) => {
			return args;
		}))
		.catch((err) => {
			logger.error(err);
			throw err;
		});
};

module.exports = async (job) => {

	logger.info('Gwent user data fetching is starting...');
	logger.info(`job: ${JSON.stringify(job)}`);

	const channel = await DiscordWatcher.client.channels.fetch(job.data.channel);


	try {
		const registeredUsers = await User.find();
		const registeredId = registeredUsers.map(user => user.id);
		const factionChangeRefusesId = registeredUsers.filter(user => user.factionChoice.faction !== factionsEnum.NONE).map(user => user.id);

		const topPlayerLimit = await getTopPlayerLimit();
		const topPlayerAmount = await getTopPlayerAmount();
		const minFactionRoleLimit = await getMinFactionRoleLimit();

		await DiscordWatcher.connect(job.data.discord_token);
		let guild = DiscordWatcher.client.guilds.cache.first();
		const members = await guild.members.fetch();


		const clients = members.map(member => {
			if (!registeredId.includes(member.user.id)) {
				return { id: member.user.id, gog: '', username: member.user.username };
			}
			else {
				return {
					id: member.user.id,
					username: member.user.username,
					gog: registeredUsers.find(user => user.id === member.user.id).gog,
				};
			}
		});

		logger.info(`${clients.length} users will be fetched soon!`);

		// if (channel) {
		// 	channel.send(`Процесс формирования статистики с сайта playgwent.com начался!`);
		// }

		const timestamp = new Date().getTime(); // all users must have equal timestamp for iteration
		let stats = []; // new stats for current
		const newcomers = []; // new entries for user structure
		const hiddenPersons = [];

		const part = 500;
		const chunkedClients = clients.reduce((all, one, i) => {
			const ch = Math.floor(i / part);
			all[ch] = [].concat((all[ch] || []), one);
			return all;
		}, []);

		for (const chunk of chunkedClients) {
			let chunkStats = await parseAllPlayers(chunk, timestamp);
			logger.info(`Process size: ${chunkStats.length}, total: ${stats.length}`);

			// process hidden guys and newcomers
			for (const entry of chunkStats) {
				if (entry.isHidden) {
					await User.findOneAndUpdate({
						id: entry.id,
						hidden: false,
					}, { $set: { hidden: true } }, { new: true });
				}

				if (entry.hasOwnProperty('newUser')) {
					newcomers.push(entry.newUser);
				}
			}

			stats = stats.concat(chunkStats);
		}


		// map all data into array
		stats = stats.filter(item => item.hasOwnProperty('data')).map(item => item.data);
		await Stats.insertMany(stats);
		logger.info(`${stats.length} users with data were saved`);

		if (newcomers.length > 0) {
			logger.info(`${newcomers.length} newcomers were added`);
			await User.insertMany(newcomers);
		}

		// update hidden field of User
		const unhidden = await User.updateMany({
			id: { $in: stats.map(e => e.player) },
			hidden: true,
		}, { $set: { hidden: false } });

		// get id of hidden data
		const hiddenId = (await User.find({ hidden: true }).select('id -_id') || []).map(e => e.id);
		//logger.info(`Hidden guys are ${hiddenId.join(', ')}`);


		//guild = await DiscordWatcher.client.guilds.cache.first().members.fetch(); // get actual list
		const hiddenMembers = members.filter(member => hiddenId.includes(member.user.id));

		logger.info(`Removing roles from ${hiddenMembers.array().length} hidden profiles`);
		for (const hidden of hiddenMembers.values()) {
			const result = await removeCustomRoles(hidden);
		}

		const latestTimestamp = await getLatestTimestamp(); // latest available timestamp (already has newest data here!)
		const topPlayerMMRList = (await getLeaderboards(profileMode.PRO, latestTimestamp, topPlayerAmount))
			.map(user => user.profile.mmr)
			.filter(score => score >= topPlayerLimit);

		if (topPlayerMMRList.length > 0) {
			logger.info(`topPlayerMMRList: ${topPlayerMMRList}`);
		}


		for (const stat of stats) {
			const member = members.find(member => member.user.id === stat.player);

			// top player role checking
			await processTopPlayers(member, stat.profile.mmr, topPlayerMMRList, topPlayerLimit);

			// faction role checking -> TODO: move it to function?

			// faction roles
			if (factionChangeRefusesId.includes(stat.player)) {
				// logger.warn(`${stat.player} set his faction!`);
				continue;
			}

			// check maximum from the total and add it
			const data = stat.wins.total;
			let factionData = Object.assign({}, data);
			delete factionData.$init;
			delete factionData.overall;

			factionData = Object.entries(factionData);

			const max = factionData.reduce((prev, cur) => {
				if (cur[1] > prev[1]) {
					return cur;
				}
				else {
					return prev;
				}
			});

			// check if role is allowed
			const factionRoles = job.data.roles.faction;
			const factions = getFactions(); // [factionsEnum.NR, factionsEnum.NG, factionsEnum.SC, factionsEnum.MO, factionsEnum.SK];

			if (max != null) {
				let desiredFaction = null;
				if (max[1] >= minFactionRoleLimit) {
					desiredFaction = max[0];
				}

				//await client.guilds.cache.first().members.find(member => member.user.id === stat.player);

				// logger.debug(max);
				// logger.debug(desiredFaction);

				if (member !== null) {
					for (const faction of Object.values(factions)) {
						if (faction === desiredFaction && !member.roles.cache.has(factionRoles[faction])) {
							const result = await member.roles.add(factionRoles[faction], `${JSON.stringify(factionData)}`);
							// logger.debug(`${member.user.username} has new role ${factionRoles[faction]}!`);
						}
						else if (faction !== desiredFaction && member.roles.cache.has(factionRoles[faction])) {
							await member.roles.remove(factionRoles[faction], `${JSON.stringify(factionData)}`);
							// logger.debug(`${member.user.username} loses role ${factionRoles[faction]}!`);
						}
					}
				}
				else {
					logger.error(`Cant find ${stat.player}!`);
				}

			}
		}
	} catch (error) {
		logger.error(`error: ${error}: stack trace: ${error.stack}`);
		db.close();

		return Promise.reject(new Error(error));
	}

	logger.info('Gwent user data fetching have been finished!');

	// if (channel) {
	// 	let result = 'Извлечение статистики игроков завершено!';
	// 	if (job.hasOwnProperty('opts') && job.opts && job.opts.hasOwnProperty('prevMillis') && job.opts.hasOwnProperty('repeat')) {
	// 		const nextTimestamp = job.opts.prevMillis + job.opts.repeat.every;
	// 		result += ` Обновление данных произойдет в ${moment(nextTimestamp).tz('Europe/Moscow').format('DD-MM-YYYY HH:mm (Z)')}`;
	// 	}
	//
	// 	channel.send(result);
	// }

	db.close();

	return Promise.resolve('ok');
};

import logger from '../logger';

const os = require('os');
const path = require('path');
const Queue = require('bull');

class QueueManager {
	constructor() {
		this.watcher = [];
		this.options = {
			removeOnComplete: true,
			removeOnFail: false,
			attempts: 20,
		};
	}

	initWatchers(options) {
		Object.entries(global.config.watchers).forEach(([_, watcher]) => {
			const queue = new Queue(watcher.queue_name, {
				redis: options,
			});
			queue.process('*', watcher.path);

			queue.on('error', (message) => logger.error(`error in queue thread: ${message}`));
			queue.on('cleaned', (message) => logger.info('watcher was cleaned: ', message));

			this.watcher.push(queue);
		});
	}

	start() {
		this.watcher.forEach(queue => {
			this.startJobs(queue);
		});
	}

	async stop() {
		for (const queue of this.watcher) {
			await this.stopJobs(queue);
		}
	}

	async stopJobs(queue) {
		const jobs = await queue.getRepeatableJobs();
		jobs.forEach(async job => await queue.removeRepeatable(`${queue.name}_job`, { every: global.config.watchers[queue.name].rate || global.config.rate }));
	}

	startJobs(queue) {
		const data = {
			discord_token: global.config.discord_token,
			bot_name: global.config.bot_name,
			...global.config.watchers[queue.name],
		};

		// additional data for specific queues
		if (queue.name === 'gwent') {
			data.roles = global.config.roles;
		}

		queue.add(`${queue.name}_job`, data, {
			...this.options, repeat: { every: global.config.watchers[queue.name].rate || global.config.rate },
		});
	}

	getQueue(name) {
		return this.watcher.find(item => item.name === name);
	}

	async getQueueJobs(queueName) {
		const queue = this.getQueue(queueName);
		if (queue) {
			const jobs = await queue.getRepeatableJobs();
			return jobs[0] || null;
		}
		else {
			return null;
		}
	}

	async getJobs() {
		const result = [];
		for (const queue of this.watcher) {
			const jobs = await queue.getRepeatableJobs();
			result.push({ queue, jobs });
		}

		return result;
	}

	startGwentTask() {
		const gwent = this.watcher.find(item => item.name === 'gwent');

		if (gwent !== null) {
			gwent.add({
				discord_token: global.config.discord_token,
				bot_name: global.config.bot_name,
				roles: global.config.roles,
				...global.config.watchers['gwent'],
			}, this.options);
		}
	}
}

export default new QueueManager();

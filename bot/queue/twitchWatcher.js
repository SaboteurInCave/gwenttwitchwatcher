import logger from '../logger';
import Streamer from '../db/streamer';
import axios from 'axios';
import { flags } from '../utils/flags';
import Database from '../utils/database';
import moment from 'moment';
import 'moment-timezone';

const Discord = require('discord.js');
const db = new Database();

// db connection

import DiscordWatcher from '../utils/discord';


const _getEmbedMessage = (twitchData, bot) => {
	return new Discord.MessageEmbed()
		.setTitle(twitchData.channel.status.length > 0 ? twitchData.channel.status : 'Stream without a name')
		.setAuthor(`${twitchData.channel.display_name}`, twitchData.channel.logo)
		.setThumbnail(twitchData.channel.logo)
		.setURL(twitchData.channel.url)
		.addField('Зрители', twitchData.viewers, true)
		.addField('Отслеживают', twitchData.channel.followers, true)
		.addField('Началось в', moment(twitchData.created_at).tz('Europe/Moscow').format('DD-MM-YYYY HH:mm'), true)
		.addField('Язык', `${flags[twitchData.channel.broadcaster_language]}`, true)
		.setImage(twitchData.preview.large)
		.setFooter(bot)
		.setColor('#823cff')
		.setTimestamp();
};


const _updateEmbed = (embed, twitchData) => {

	// viewers
	if (embed.fields[0].value !== twitchData.viewers) {
		embed.fields[0].value = twitchData.viewers;
	}

	// followers
	if (embed.fields[1].value !== twitchData.channel.followers) {
		embed.fields[1].value = twitchData.channel.followers;
	}

	// started in
	if (embed.fields[2].value !== twitchData.created_at) {
		embed.fields[2].value = moment(twitchData.created_at).tz('Europe/Moscow').format('DD-MM-YYYY HH:mm');
	}

	// language
	if (embed.fields[3].value !== twitchData.channel.broadcaster_language) {
		embed.fields[3].value = `${flags[twitchData.channel.broadcaster_language]}`;
	}

	// thumbnail
	if (embed.title !== twitchData.channel.status) {
		embed.title = twitchData.channel.status.length > 0 ? twitchData.channel.status : 'Stream without a name';
	}

	//embed.image.url = twitchData.preview.large;
	//embed.image.proxy_url = twitchData.preview.large;
	return embed;

};

module.exports = async (job) => {


	try {
		await DiscordWatcher.connect(job.data.discord_token);
		const channel = await DiscordWatcher.client.channels.fetch(job.data.channel);

		if (channel !== null) {
			// logger.debug('channel was founded... ' + channel.name);

			const activeStreamers = await Streamer.find();
			const mongoStreamers = activeStreamers.map(streamer => {
				return { id: streamer.id, message: streamer.message };
			});

			// logger.debug(mongoStreamers);
			// logger.debug(`TWITCH-token: ${job.data.twitch_token}`);

			const response = await axios.get('https://api.twitch.tv/kraken/streams', {
				params: { game: 'Gwent: The Witcher Card Game' },
				headers: { 'Accept': 'application/vnd.twitchtv.v5+json', 'Client-ID': job.data.twitch_token },
			});

			// logger.debug('Twitch response is here');

			if ('streams' in response.data) {
				// remove non existed streamers from mongo
				let twitchStreamers = response.data.streams;

				// check filters
				if (job.data.languages.length > 0) {
					twitchStreamers = twitchStreamers.filter(streamer => job.data.languages.includes(streamer.channel.broadcaster_language));
				}

				if (job.data.strict_mode) {
					twitchStreamers = twitchStreamers.filter(streamer => job.data.allowed_streamers.includes(streamer.channel.name));
				}

				const twitchId = twitchStreamers.map(streamer => streamer._id);
				const removedStreamer = mongoStreamers.filter(streamer => !twitchId.some(s => s === streamer.id));

				// message removing
				for (const streamer of removedStreamer) {
					// logger.debug(`streamer left: ${streamer}`);
					try {
						const message = await channel.messages.fetch(streamer.message);
						if (message) {
							await message.delete();
						}
					} catch (e) {
						logger.error('Cant find message for specific streamer!');
					}
				}

				// batch remove
				await Streamer.deleteMany({ id: { $in: removedStreamer.map(s => s.id) } });

				const newStreamers = [];

				// check twitch streamers and mongo data
				for (const onlineStreamer of twitchStreamers) {
					const existedStreamer = mongoStreamers.some(streamer => streamer.id === onlineStreamer._id);

					// logger.debug(`streamer: ${onlineStreamer.channel.display_name} - ${onlineStreamer._id}: ${existedStreamer}`);

					if (!existedStreamer) {
						// add it as message and save to mongo
						if (channel != null) {
							const message = await channel.send(_getEmbedMessage(onlineStreamer, job.data.bot_name));
							newStreamers.push(new Streamer({
								id: onlineStreamer._id,
								message: message.id,
							}));
						}
						else {
							logger.error('Channel reference was deleted!');
						}

					}
					else {
						// update data here?
						const currentStreamer = mongoStreamers.find(streamer => streamer.id === onlineStreamer._id);
						const message = await channel.messages.fetch(currentStreamer.message);

						if (message) {
							// it is more correct to update image after embed creation
							await message.edit(new Discord.MessageEmbed(_updateEmbed(message.embeds[0], onlineStreamer)));
						}
					}

				}

				if (newStreamers.length > 0) {
					const result = await Streamer.insertMany(newStreamers);
				}
			}

		}
		else {
			db.close();
			return Promise.reject(new Error('Can\'t find any channel with given id'));
		}

		db.close();
		return Promise.resolve('ok');
	} catch (error) {
		db.close();
		logger.error(`twitch error: ${error}`);
		return Promise.reject(new Error(error));
	}
};

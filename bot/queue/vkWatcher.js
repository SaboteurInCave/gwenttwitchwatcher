import Database from '../utils/database';
import Posts from '../db/posts';

import logger from '../logger';
import axios from 'axios';
import DiscordWatcher from '../utils/discord';

const Discord = require('discord.js');
const db = new Database();

const strip_data = (text) => {
	const brackets = /\[#(\w+|[а-яА-ЯЁё]+)|#(\w+|[а-яА-ЯЁё]+)@(\w+|[а-яА-ЯЁё]+)\]/gmu;

	const result = brackets.exec(text);

	const lastLine = text.lastIndexOf('\n');

	if (lastLine > -1) {
		// get hashtags and cut content part
		let content = text.substring(0, lastLine);
		let hashtags = text.substring(lastLine, text.length);
		const diff = text.length - lastLine;

		while (content.length + diff + 4 >= 2048) {
			content = content.substring(0, content.lastIndexOf(' '));
		}

		return `${content}...\n${hashtags}`;
	}
	else {
		// just cut content part
		while (text.length + 4 >= 2048) {
			text = text.substring(0, text.lastIndexOf(' '));
		}

		return `${text}...\n`;
	}
};

const _getEmbedMessage = (data, job) => {
	// add content from attachments

	const message = new Discord.MessageEmbed()
		.setTitle('Claymore новости!')
		.setAuthor(job.data.group_name)
		.setThumbnail(job.data.group_image)
		.setDescription(data.text)
		.setURL(`https://vk.com/claymoregwent?w=wall${job.data.group_id}_${data.id}`)
		.setFooter(job.data.bot_name)
		.setColor('#bb4cff')
		.setTimestamp();

	if (data.hasOwnProperty('attachments')) {
		// logger.debug(data.attachments);
		const images = data.attachments.filter(item => item.type === 'photo');
		if (images.length > 0) {

			const image = images[0];
			const maxImage = image.photo.sizes.reduce((prev, current) => prev.width > current.width ? prev : current);

			if (maxImage) {
				message.setImage(maxImage.url);
			}
		}
	}

	return message;
};

const findChannelId = (job, text) => {
	let channel = null;
	for (const tagEntry of job.data.tags) {
		if (tagEntry.tags.some(tag => text.includes(tag))) {
			channel = job.data.channels[tagEntry.channel];
			break;
		}
	}

	return channel;
};

module.exports = async (job) => {
	const type = 'vk';


	await DiscordWatcher.connect(job.data.discord_token);

	const hashtagRegex = /#(\w+|[а-яА-ЯЁё]+)(\s|$)/gmu;
	const hashtagGroupRegex = /#(\w+|[а-яА-ЯЁё]+)@(\w+|[а-яА-ЯЁё]+)/gmu;
	const linkRegex = /\[([^|]*)\|([^|]*)\]/gmu;

	try {
		// logger.info('vk fetching is started');

		let vkData = await Posts.findOne({ type });

		if (!vkData) {
			// need to create some stuff here
			await Posts.create({
				type,
				latest: (new Date().getTime()) - (24 * 60 * 60 * 1000),
				posts: [],
			});

			vkData = await Posts.findOne({ type });
		}

		let latest = vkData.latest;

		const response = await axios.get('https://api.vk.com/method/wall.get', {
			params: {
				access_token: job.data.vk_token,
				owner_id: job.data.group_id,
				count: 5,
				v: '5.107',
			},
		});

		const usedPosts = vkData ? vkData.posts.map(item => item.post_id) : [];

		if (response.data.hasOwnProperty('response')) {
			let items = 'items' in response.data.response ? response.data.response.items : [];
			items = items.filter(item => !usedPosts.includes(item.id));

			for (const entry of items) {
				const postTimestamp = entry.date * 1000;
				// logger.info(`New vk post with id: ${entry.id} with timestamp: ${postTimestamp}`);

				// find proper channel for posting this thing
				const channelId = findChannelId(job, entry.text);

				if (channelId) {
					const channel = await DiscordWatcher.client.channels.fetch(channelId);

					if (channel) {
						// logger.debug(entry);

						// post  embed data to discord
						entry.text = entry.text
							.replace(linkRegex, '[$2](https://vk.com/$1)')
							.replace(hashtagGroupRegex, '[#$1@$2](https://vk.com/$2/$1)')
							.replace(hashtagRegex, '[#$1](https://vk.com/feed?q=%23$1&section=search)$2');

						if (entry.text.length > 2048) {
							entry.text = strip_data(entry.text);
						}

						// logger.info('vk post text processed');

						const result = await channel.send(_getEmbedMessage(entry, job));

						logger.info(`Posting result: ${result}`);

						// update post instance
						vkData.posts.push({
							post_id: entry.id,
							timestamp: postTimestamp,
						});

						// logger.info(`Post ${entry.id} have been pushed for saving`)
					}
					else {
						logger.error(`Channel with id ${channelId} not found!`);
					}
				}
				// else {
				// 	logger.error(`Channel not found for the given text!`);
				// }
			}

			// TODO: update latest timestamp in
			// while (vkData.posts.length > 10) {
			// 	const oldest = vkData.posts.reduce((min, p) => p.timestamp < min ? p.timestamp : min, vkData.posts[0].timestamp);
			// 	vkData.posts = vkData.posts.filter(item => item.timestamp !== oldest);
			// }

			vkData.latest = (new Date().getTime());

			await vkData.save();
		}
		else {
			logger.error('VK hasn\'t send response :(');
			logger.error(response.data);
		}
	} catch (e) {
		logger.error(JSON.stringify(e));
		db.close();
		return Promise.reject(new Error(e));
	}

	// logger.info('vk fetching is finished');

	db.close();
	return Promise.resolve('ok');
};

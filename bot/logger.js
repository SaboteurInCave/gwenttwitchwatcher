const { createLogger, format, transports } = require('winston');
const util = require('util');

export default createLogger({
	level: 'debug',
	format: format.combine(
		format.colorize(),
		format.splat(),
		format.timestamp({
			format: 'YYYY-MM-DD HH:mm:ss',
		}),
		format.printf(info => `${info.timestamp} ${info.level}: ${util.inspect(info.message, { depth: 6 })}`),
	),
	transports: [new transports.Console()],
});

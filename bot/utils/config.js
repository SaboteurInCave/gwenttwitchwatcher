export const loadConfig = () => {
	if (process.env.NODE_ENV === 'production') {
		return './config';
	}
	else if (process.env.NODE_ENV === 'development') {
		return './config.dev';
	}
};
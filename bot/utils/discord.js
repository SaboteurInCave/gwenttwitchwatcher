import logger from '../logger';
import fs from 'fs';
import { argsEnum } from '../enums';
import { Intents } from 'discord.js';

const Discord = require('discord.js');
const path = require('path');


class DiscordWatcher {
	constructor() {
		logger.debug('Init discord watcher');
		this.watchers = {};
		this._init();
	}

	_init() {
		this.client = new Discord.Client({ ws: { intents: Intents.ALL } });
		this.client.on('error', error => {
			logger.error('The WebSocket encountered an error:', error);
		});
	}

	setCommands() {
		this.client.commands = new Discord.Collection();

		// read all commands
		const commandFiles = fs.readdirSync(path.resolve(path.normalize(__dirname + '/..'), 'commands')).filter(file => file.endsWith('.js'));

		for (const file of commandFiles) {
			const command = require(path.resolve(path.normalize(__dirname + '/..'), 'commands', file));
			this.client.commands.set(command.name, command);
		}


		this.client.on('message', message => {
			if (!message.content.startsWith(config.prefix) || message.author.bot) return;

			const args = message.content.slice(config.prefix.length).split(/ +/);
			const commandName = args.shift().toLowerCase();

			const command = this.client.commands.get(commandName)
				|| this.client.commands.find(cmd => cmd.aliases && cmd.aliases.includes(commandName));

			if (!command) return;

			if ('args' in command && command.args === argsEnum.REQUIRED && !args.length) {
				let reply = `Не предоставлены необходимые аргументы к команде!, ${message.author}!`;

				if (command.usage) {
					reply += `\nПравильное использование: \` ${command.usage}\``;
				}

				return message.channel.send(reply); //.then(replyed => replyed.delete(10000));
			}

			try {
				command.execute(message, args);
			} catch (error) {
				logger.error(error);
				message.reply(`Возникла ошибка при исполнении команды!\n${error}`); //.then(replyed => replyed.delete(10000));
			}
		});

	}

	/**
	 *
	 * @param token
	 * @returns {Promise<string>}
	 */
	async connect(token) {
		// logger.debug('Connection discord watcher');

		if (this.client.readyAt === null) {
			return await this.client.login(token);
		}
		else {
			return Promise.resolve('ok');
		}
	}


	/*
	getClient(queue) {
		logger.debug(queue);
		logger.debug(this.watchers);
		return this.watchers[queue];
	}
	*/
}

export default new DiscordWatcher();

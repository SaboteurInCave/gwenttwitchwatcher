import User from '../db/user';
import logger from '../logger';

export const getPermissions = (message) => {
	if (message.member.hasPermission('ADMINISTRATOR')) {
		return true;
	}

	if (global.config.groups.length === 0) {
		return false;
	}

	for (const role_id of global.config.groups) {
		const discordRole = message.guild.roles.cache.get(role_id);
		if (discordRole) {
			if (message.member.roles.cache.has(discordRole.id)) {
				return true;
			}
		}
	}

	return false;
};

export const isUserHidden = async (userId) => {
	let userEntry = await User.findOne({ id: userId });
	return userEntry !== null && 'hidden' in userEntry && userEntry.hidden;
};

import logger from '../logger';
let mongoose = require('mongoose');

export default class Database {

	constructor() {
		this.type = process.env.NODE_ENV || 'development';
		this.connection = this._connect();
	}

	_connect() {
		const options = {
			useNewUrlParser: true,
		};

		if (this.type === 'production') {
			options.user = process.env.MONGO_INITDB_ROOT_USERNAME;
			options.pass = process.env.MONGO_INITDB_ROOT_PASSWORD;
		}

		const mongooseConnection = mongoose.connect(`mongodb://${process.env.MONDO_ADDRESS}:${process.env.MONGO_PORT}/${process.env.MONGO_DATABASE}`, options)
			.then(() => logger.info('Database connection successful')).catch(() => logger.error('Database connection error'));

		return mongooseConnection.connection
	}

	async close() {
		//logger.debug('connection is going to close');
		// logger.debug(this.connection);
		//await this.connection.close();
		// logger.debug('connection is closed');
	}
}

const path = require('path');
const roles = require(path.resolve(path.normalize(__dirname + '/..'), 'configs', 'roles.js'));
import logger from '../logger';


export const getRoles = () => {
	const discord_roles = [];

	// faction roles
	for (const factionRoles of Object.values(roles.faction)) {
		discord_roles.push(factionRoles);
	}

	// TODO: add new roles here!

	// top player role
	discord_roles.push(roles.topPlayer);

	return discord_roles;
};

export const getTopPlayerRole = () => {
	return roles.topPlayer
};

export const removeCustomRoles = async (userMember, customRoles = getRoles(), reason = `${userMember.user.username} скрыл профиль - удаляем выданные роли`) => {
	if (userMember) {
		const memberRoles = userMember.roles.cache.filter(role => customRoles.includes(role.id));
		// TODO: check if v12 now correctly remove roles array only with 1 item
		try {
			if (memberRoles.length === 1) {
				await userMember.roles.remove(memberRoles[0], reason);
			}
			else {
				await userMember.roles.remove(memberRoles, reason);
			}
		}
		catch (e) {
			logger.error(e);
			throw e
		}

	}
};

import Settings from '../db/settings';
import logger from '../logger';

export const settingsText = `
1. topPlayerLimit (число) - нижняя граница mmr для топовых игроков
2. topPlayerAmount (число) - общее количество игроков с ролью "топовый игрок"
3. minFactionRoleLimit (число) - минимальное количество побед для фракционной роли`;

export const getSettings = async (field) => {
	let settings = await Settings.findOne({});
	if (!settings) {
		settings = await Settings.create({});
	}

	if (field) {
		// only one specified field
		if (field in settings) {
			return settings[field];
		}
		else {
			throw `Не удалось найти настройку ${field}!`;
		}
	} else {
		// all fields
		let result = '';
		const fields = Object.keys(Settings.schema.obj);
		for (field of fields) {
			result += `${field}: ${settings[field]}\n`
		}

		return result
	}


};

export const setSettings = async (field, value) => {
	let settings = await Settings.findOne({});
	if (!settings) {
		settings = await Settings.create({});
	}

	if (field in settings) {
		settings[field] = value;
		await settings.save();
	}
	else {
		throw `Не удалось найти настройку ${field}!`;
	}
};

export const getTopPlayerLimit = async () => {
	return await getSettings('topPlayerLimit');
};

export const getMinFactionRoleLimit = async () => {
	return await getSettings('minFactionRoleLimit');
};

export const getTopPlayerAmount = async () => {
	// topPlayerAmount
	return await getSettings('topPlayerAmount');
};

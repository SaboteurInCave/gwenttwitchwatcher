import { CanvasRenderService } from 'chartjs-node-canvas';
import { factionsEnum } from '../enums';

// 					// await message.reply(makeEmbed(result, timestamp, usedAccounts.size));

const width = 1920;
const height = 1080;
export const plotLegendFontSize = 28;
export const plotLabelFontSize = 24;

const chartJsFactory = () => {
	const Chart = require('chart.js');
	require('chartjs-plugin-datalabels');
	delete require.cache[require.resolve('chart.js')];
	delete require.cache[require.resolve('chartjs-plugin-datalabels')];
	return Chart;
};

const canvasRenderService = new CanvasRenderService(width, height, (ChartJS) => {
	ChartJS.plugins.register({
		beforeDraw: (chart, options) => {
			const ctx = chart.ctx;
			ctx.save();
			ctx.fillStyle = '#ffffff';
			ctx.fillRect(0, 0, width, height);
			ctx.restore();
		},
		// beforeInit: function(chart, options) {
		// 	chart.legend.afterFit = function() {
		// 		this.height = this.height + 50;
		// 	};
		// },
	});

}, null, chartJsFactory);

export const renderChart = (configuration) => canvasRenderService.renderToBuffer(configuration);

export const factionColors = {
	[factionsEnum.NR]: {
		borderColor: '#2196F3',
		backgroundColor: '#1976D2',
	},
	[factionsEnum.NG]: {
		borderColor: '#212121',
		backgroundColor: '#FFFF00',
	},
	[factionsEnum.SC]: {
		borderColor: '#4CAF50',
		backgroundColor: '#388E3C',
	},
	[factionsEnum.MO]: {
		borderColor: '#F44336',
		backgroundColor: '#D32F2F',
	},
	[factionsEnum.SK]: {
		borderColor: '#9C27B0',
		backgroundColor: '#7B1FA2',
	},
	[factionsEnum.SY]: {
		borderColor: '#FF9800',
		backgroundColor: '#F57C00',
	},
};

export const factionLocalization = {
	[factionsEnum.NR]: 'NR',
	[factionsEnum.NG]: 'NG',
	[factionsEnum.SC]: 'ST',
	[factionsEnum.MO]: 'MO',
	[factionsEnum.SK]: 'SK',
	[factionsEnum.SY]: 'SY',
};

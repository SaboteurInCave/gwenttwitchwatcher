import Stats from '../db/stats';
import User from '../db/user';
import logger from '../logger';


/**
 * Get discord user from Discord or GOG username
 * @param desiredUser - Discord User Object or string with user name for gog and discord searching
 * @param users - list of discord users
 */
export const getUsers = async (desiredUser, users) => {
	const result = [];
	const idSet = new Set();
	let isDiscordNeeded = true;

	// discord processing
	let playerId = -1;

	if (typeof desiredUser === 'string') {
		const discordUser = users.find(user => user.username.toLowerCase() === desiredUser.toLowerCase());
		if (discordUser) {
			playerId = discordUser.id;
		}
	}

	const data = await Stats.find({ player: typeof desiredUser === 'string' ? playerId : desiredUser.id }).sort('-timestamp').limit(1);
	if (data.length > 0) {
		result.push(data[0]);
		idSet.add(data[0].player);
	}

	// gog processing
	const gogName = typeof desiredUser === 'object' ? desiredUser.username : desiredUser;
	const regex = new RegExp(`^${gogName}$`, 'i');
	const gogEntry = await User.find({gog: regex });

	if (gogEntry.length > 0) {
		let gogData = await Stats.find({ player: gogEntry[0].id }).sort('-timestamp').limit(1);
		if (gogData.length > 0 && !idSet.has(gogData[0].player)) {
			result.push(gogData[0]);
		}
	}

	return result;
};


import axios from 'axios';
import Stats from '../db/stats';
import logger from '../logger';
import { MessageAttachment } from 'discord.js';

export const getLatestTimestamp = async () => (await Stats.find().distinct('timestamp')).slice(-1)[0];

const getPositionDifference = (a, b) => {
	if (a.profile.mmr !== b.profile.mmr) {
		return b.profile.mmr - a.profile.mmr;
	}
	else if (a.profile.position !== b.profile.position) {
		return a.profile.position - b.profile.position;
	}
	else {
		return a.profile.name.localeCompare(b.profile.name);
	}
};

export const getLeaderboards = async (mode, latestTimestamp, limit = 10) => {
	if (latestTimestamp !== null) {

		const users = await Stats.find({
			timestamp: latestTimestamp,
			'profile.position': { $gt: 0 },
			'profile.mode': mode,
		});

		const uniqueNames = new Set();
		let uniqueUsers = [];

		for (const user of users) {
			if (!uniqueNames.has(user.profile.name)) {
				uniqueNames.add(user.profile.name);
				uniqueUsers.push(user);
			}
		}

		uniqueUsers = uniqueUsers
			.sort((a, b) => getPositionDifference(a, b))
			.slice(0, limit);

		return uniqueUsers;
	}
	else {
		return [];
	}
};

export const setPlayerImage = async (message, profile) => {
	try {
		let backendURL = `${global.config.backend_address}/api/user_image/`

		if (profile.avatar) {
			backendURL += `${profile.avatar}/`
		}

		if (profile.border) {
			backendURL += `${profile.border}/`
		}

		const response = await axios.request({
			responseType: 'arraybuffer',
			url: backendURL,
			method: 'get',
			headers: {
				'Content-Type': 'image/png',
			},
			timeout: 2000,
		});

		const attachmentName = `${profile.avatar}_${profile.border}.png`;
		const attachment = new MessageAttachment(response.data, attachmentName);
		message.attachFiles([attachment]);
		message.setThumbnail(`attachment://${attachmentName}`);
	} catch (e) {
		logger.error(e.stack);
		message.setThumbnail(`https://cdn-l-playgwent.cdprojektred.com/avatars/${profile.avatar}.jpg`);
	}
};

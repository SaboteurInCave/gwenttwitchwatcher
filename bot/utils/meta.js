import { getFactions } from '../utils';
import logger from '../logger';
import { factionLocalization } from './canvas';

const Table = require('easy-table');

export const regularLadderParameter = 'regular';
export const minimalChartParameter = 'min';

export const parseRegularLadderRange = (args) => {
	const failMessage = 'Не удалось определить границы рангов для рейтинговой таблицы.';
	if (args.length === 1) {
		return {
			ranks: [1, 30],
			range: { $gte: 1, $lte: 30 },
		};
	}
	else {
		const rangeRaw = args[1];
		try {
			const ranks = rangeRaw.split('-').map(item => parseInt(item));

			if (ranks.length !== 2 || ranks.some(item => Number.isNaN(item))) {
				throw failMessage;
			}

			if (ranks[0] > ranks[1]) {
				[ranks[0], ranks[1]] = [ranks[1], ranks[0]];
			}

			return {
				ranks: ranks,
				range: { $gte: ranks[0], $lte: ranks[1] },
			};

		} catch (e) {
			logger.error(e);
			throw failMessage;
		}
	}
};

export const makeGamesTable = (data) => {

	const table = new Table();

	data = Object.entries(data).sort(((a, b) => {
		const winrateDiff = b[1].winrate - a[1].winrate;
		return winrateDiff !== 0 ? winrateDiff : b[1].total - a[1].total;
	}));

	for (const faction of data) {
		table.cell('Фракция', factionLocalization[faction[0]]);
		table.cell('Playrate', `${faction[1].playrate.toFixed(3)} %`);
		table.cell('Кол-во игр', faction[1].total);
		table.cell('Winrate', `${faction[1].winrate.toFixed(3)} %`);
		table.newRow();
	}

	return `\`\`\`${table.toString()}\`\`\``;
};

export const getFactionsMetaTable = (factions) => {
	const result = {};
	for (const faction of Object.values(factions)) {
		result[faction] = { wins: 0, total: 0, winrate: 0 };
	}

	return result;
};

export const getMetaWinrateData = (stats) => {
	const factions = getFactions();
	const result = getFactionsMetaTable(factions);
	const usedAccounts = new Set();


	for (const entry of stats) {
		if (!usedAccounts.has(entry.profile.name)) {
			usedAccounts.add(entry.profile.name);

			for (const faction of Object.values(factions)) {
				if (result.hasOwnProperty(faction)) {
					result[faction].wins += entry.wins.season[faction];
					result[faction].total += entry.season.factions[faction].games;

					const winrate = result[faction].total === 0 ? 0 : result[faction].wins / result[faction].total;
					result[faction].winrate = winrate * 100;
				}
			}
		}
	}

	const totalAmount = Object.values(result).reduce((a, b) => a + (b.total || 0), 0);

	for (const faction of Object.values(factions)) {
		result[faction].playrate = totalAmount === 0 ? 0 : (result[faction].total / totalAmount) * 100;
	}

	return { result, usedAccounts };
};

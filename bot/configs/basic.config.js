module.exports = {
	// commands will start with
	prefix: '!',
	// bot name for all embed messages
	bot_name: 'GwentTwitchWatcher',
	// users from groups (+ admin) are allowed to interact with bot
	groups: ['Moderators'],
	// update rate (ms)
	rate: 30000,

	// removing rate of messages
	removeRate: 30000,

	// backend address
	backend_address: process.env.BACKEND_ADDRESS,

	// internal discord token
	discord_token: process.env.DISCORD_TOKEN,

	// internal storage for watchers
	watchers: {}
};

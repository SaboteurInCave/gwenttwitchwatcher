import fs from 'fs';
import Database from './utils/database';
import logger from './logger';
import QueueManager from './queue/queueManager';
import { parseTotalWins } from './utils';
import DiscordWatcher from './utils/discord';

const cheerio = require('cheerio');

let mongoose = require('mongoose');
require('dotenv').config();

// read config files
const configFiles = fs.readdirSync('./configs').filter(file => file.endsWith('.config.js'));
global.config = {};

for (const file of configFiles) {
	global.config = { ...global.config, ...require(`./configs/${file}`) };
}

// add roles to the user
if (fs.existsSync('./configs/roles.js')) {
	global.config.roles = require(`./configs/roles.js`);
}

// read watcher config files
const watcherFiles = fs.readdirSync('./configs').filter(file => file.endsWith('.watcher.js'));

for (const file of watcherFiles) {
	global.config.watchers[file.replace('.watcher.js', '')] = require(`./configs/${file}`);
}

logger.debug(global.config);

QueueManager.initWatchers({
	host: process.env.REDIS_ADDRESS || 'localhost',
	port: process.env.REDIS_PORT || 6379,
	password: process.env.REDIS_PASSWORD || '',
});

const db = new Database();

// discord staff

DiscordWatcher.setCommands();
DiscordWatcher.connect(global.config.discord_token).then(message => logger.debug(message));

logger.info('Bot have been started');

process.on('SIGTERM', async () => {
	logger.info('App is closing...');
	mongoose.connection.close();
	await QueueManager.stop();
});

process.on('unhandledRejection', (reason, p) => {
	logger.error(`Unhandled promise rejection! Promise: ${p}, reason: ${reason}`);
});

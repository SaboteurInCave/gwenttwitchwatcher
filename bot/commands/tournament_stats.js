import { getPermissions } from '../utils/permissions';
import axios from 'axios';
import logger from '../logger';

const Discord = require('discord.js');
import { MessageAttachment } from "discord.js";

module.exports = {
	'name': 'tournament_stats',
	'description': 'Получение турнирной статистики',
	'usage': `${global.config.prefix}tournamentStats`,
	'aliases': ['tournamentstats'],
	async execute(message, args) {
		if (!getPermissions(message)) {
			return message.reply('Недостаточно прав для использования команды!');
		}

		if (args.length === 0) {
			return message.reply('Введите ID турнира');
		}

		try {
			const response = await axios.request({
				responseType: 'arraybuffer',
				url: `${global.config.backend_address}/api/tournament/${args[0]}`,
				method: 'get',
				headers: {
					'Content-Type': 'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet',
				},
				timeout: 10000,
			});


			const filename = response.headers['content-disposition'].split("filename=")[1];
			const attachment = new MessageAttachment(response.data, filename.substring(1, filename.length - 1));

			return message.channel.send(`${message.author}`, attachment)
		} catch (e) {
			logger.error(e.stack);
			return message.reply('Произошла ошибка во время извлечения статистики турнира, обратитесь к разработчикам за логом')
		}


	},
};

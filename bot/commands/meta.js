import { argsEnum, profileMode } from '../enums';
import Stats from '../db/stats';
import { formateDate } from '../utils';
import { getMetaWinrateData, makeGamesTable, parseRegularLadderRange, regularLadderParameter } from '../utils/meta';
import logger from '../logger';

const Discord = require('discord.js');

const makeEmbed = (data, timestamp, usersCount, label) => {
	return new Discord.MessageEmbed()
		.setTitle(`Винрейт фракций (${label})`)
		.setDescription(`Данные за *${formateDate(timestamp)}*, количество игроков: ${usersCount}`)
		.addField(`Данные:`, makeGamesTable(data))
		.setFooter(global.config.bot_name)
		.setTimestamp();
};

module.exports = {
	'name': 'meta',
	'description': 'Получение винрейтов фракций',
	'args': argsEnum.OPTIONAL,
	'usage': `${global.config.prefix}meta, ${global.config.prefix}meta regular *rank_a*-*rank_b* (get faction winrate in regular ladder in ranks [rank_a, rank_b])>`,
	async execute(message, args) {
		try {
			const latestTimestamp = (await Stats.find().distinct('timestamp')).slice(-1)[0];
			const query = { timestamp: latestTimestamp };
			let ranks = [0, 0];

			let mode = profileMode.PRO;

			if (args.length > 0 && args[0].toLowerCase() === regularLadderParameter) {
				mode = profileMode.REGULAR;

				try {
					const parsedRanks = parseRegularLadderRange(args);
					ranks = parsedRanks.ranks;
					query['profile.rank'] = parsedRanks.range;
				} catch (errorMessage) {
					return message.reply(errorMessage);
				}
			}

			query['profile.mode'] = mode;

			const stats = await Stats.find(query);
			const { result, usedAccounts } = getMetaWinrateData(stats);

			const label = mode === profileMode.PRO ? 'про-ранг' : `рейтинг, ранг: ${ranks[0]} ... ${ranks[1]}`;

			message.reply(makeEmbed(result, latestTimestamp, usedAccounts.size, label));
		} catch (e) {
			logger.error(e);
		}
	},
};

import { getPermissions } from '../utils/permissions';
import QueueManager from '../queue/queueManager';

module.exports = {
	'name': 'start_stream_watching',
	'description': 'Start stream watching in the current room',
	async execute(message) {
		if (!getPermissions(message)) {
			return message.reply('Недостаточно прав для использования команды!') //.then(e => e.delete(global.config.removeRate));
		}

		QueueManager.start();
		return message.reply('Началась работа по отслеживанию стримов и статистики!')
	},
};

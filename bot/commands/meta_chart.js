import { argsEnum, factionsEnum, profileMode } from '../enums';
import {
	getMetaWinrateData,
	makeGamesTable,
	minimalChartParameter,
	parseRegularLadderRange,
	regularLadderParameter,
} from '../utils/meta';
import { formateDate, getFactions } from '../utils';
import Stats from '../db/stats';
import logger from '../logger';

import moment from 'moment';
import 'moment-timezone';

import {
	factionColors,
	factionLocalization,
	plotLabelFontSize,
	plotLegendFontSize,
	renderChart,
} from '../utils/canvas';

const Discord = require('discord.js');

const makeEmbed = (data, timestamp, usersCount) => {
	return new Discord.MessageEmbed()
		.setTitle(`Винрейт фракций (про-ранг)`)
		.setDescription(`Данные за *${formateDate(timestamp)}*, количество игроков: ${usersCount}`)
		.addField(`Данные:`, makeGamesTable(data))
		.setFooter(global.config.bot_name)
		.setTimestamp();
};

const datetimePad = (value) => String('00' + value).slice(-2);

module.exports = {
	'name': 'meta_chart',
	'description': 'Получение графика изменения винрейта фракций',
	'args': argsEnum.OPTIONAL,
	'usage': `${global.config.prefix}meta_chart или ${global.config.prefix}meta_chart ${regularLadderParameter} *num_a*-*num_b* (optional)`,
	'aliases': ['metachart'],
	async execute(message, args) {

		// get range from first day of month to the last day of month
		const currentDay = new Date();
		const first = new Date(currentDay.getFullYear(), currentDay.getMonth(), 1, 0, 0, 0);
		const last = new Date(currentDay.getFullYear(), currentDay.getMonth() + 1, 0, 23, 59, 59);
		const factions = Object.values(getFactions());

		let mode = profileMode.PRO;
		let ranks = [0, 0];
		const query = {};

		// check minimal chart
		const isMinimalChart = args.find(arg => arg.toLowerCase() === minimalChartParameter) !== undefined;
		if (isMinimalChart) {
			args = args.filter(item => item.toLowerCase() !== minimalChartParameter);
		}

		let usedFactions = [];

		for (let [index, arg] of args.entries()) {
			if (arg.toLowerCase() === factionLocalization[factionsEnum.SC].toLowerCase()) {
				args[index] = factionsEnum.SC;
			}
		}

		for (let faction of factions) {
			const isFactionPresented = args.find(arg => arg.toLowerCase() === faction.toLowerCase()) !== undefined;
			if (isFactionPresented) {
				usedFactions.push(faction);
				args = args.filter(item => item.toLowerCase() !== faction.toLowerCase());
			}
		}

		if (usedFactions.length === 0) {
			usedFactions = factions;
		}


		if (args.length > 0 && args[0].toLowerCase() === regularLadderParameter) {
			mode = profileMode.REGULAR;

			try {
				const parsedRanks = parseRegularLadderRange(args);
				ranks = parsedRanks.ranks;
				query['profile.rank'] = parsedRanks.range;
			} catch (errorMessage) {
				return message.reply(errorMessage);
			}
		}

		query['profile.mode'] = mode;

		try {
			let timestamps = await Stats.distinct('timestamp', {
				timestamp: {
					$gte: first.getTime(),
					$lt: last.getTime(),
				},
			}); //.sort('+timestamp');

			timestamps = timestamps.sort((a, b) => a - b);
			const dataBrackets = {};

			// logger.info(timestamps.map(item => new Date(item)));

			for (const entry of timestamps) {
				const vanillaDatetime = new Date(entry);
				const datetime = new moment(vanillaDatetime).tz('Europe/Moscow');
				const bracket = `${datetime.year()}-${datetimePad(datetime.month() + 1)}-${datetimePad(datetime.date())}`;

				if (!dataBrackets.hasOwnProperty(bracket)) {
					dataBrackets[bracket] = null;
				}

				dataBrackets[bracket] = vanillaDatetime.getTime();
			}

			// logger.info(dataBrackets);

			const entries = await Stats.find({
				...query,
				timestamp: { $gte: first.getTime(), $lte: last.getTime(), $in: Object.values(dataBrackets) },
			}).sort('+timestamp');

			const metaResults = {};
			let maxUsers = 0;

			try {
				for (const timestamp of Object.values(dataBrackets)) {
					const users = entries.filter(user => user.timestamp === timestamp);
					const { result, usedAccounts } = getMetaWinrateData(users);

					maxUsers = Math.max(maxUsers, usedAccounts.size);
					metaResults[timestamp] = result;
				}
			} catch (e) {
				logger.error(e);
			}

			const labels = Object.values(dataBrackets).map(item => moment(new Date(item)).tz('Europe/Moscow').format('DD.MM'));


			const datasets = usedFactions.map(faction => ({
				label: factionLocalization[faction],
				...factionColors[faction],
				fill: false,
				data: [],
			}));

			for (const entry of Object.values(metaResults)) {
				for (const [index, faction] of usedFactions.entries()) {
					datasets[index].data.push(entry[faction].winrate.toFixed(3));
				}
			}

			const label = mode === profileMode.PRO ? 'pro-ladder' : `regular ladder (${ranks[0]} ... ${ranks[1]} ranks)`;
			const chartPlugins = {
				datalabels: {
					backgroundColor: '#EEEEEE',
					borderWidth: 1,
					anchor: 'top',
					align: 'top',
					offset: 6,
					display: 'auto',
					font: {
						size: plotLabelFontSize,
					},
				},

			};

			const configuration = {
				type: 'line',
				data: {
					labels,
					datasets,
				},
				options: {
					title: {
						display: true,
						text: `Faction winrate in ${label} for ${moment(first).format('MMMM')} (player: ${maxUsers})`,
						fontSize: plotLegendFontSize,
					},
					legend: {
						position: 'bottom',
						labels: {
							fontSize: plotLabelFontSize,
							padding: 25,
						},
					},
					scales: {
						xAxes: [
							{
								offset: true,
								scaleLabel: {
									display: true,
									labelString: 'Date',
									fontSize: plotLegendFontSize,
								},
								ticks: {
									fontSize: plotLabelFontSize,
								},
							}],
						yAxes: [{
							scaleLabel: {
								display: true,
								labelString: 'Winrate (%)',
								fontSize: plotLegendFontSize,
							},
							ticks: {
								fontSize: plotLabelFontSize,
							},
						}],
					},
					plugins: {
						datalabels: {
							backgroundColor: '#EEEEEE',
							borderWidth: 1,
							anchor: 'top',
							align: 'top',
							offset: 6,
							display: !isMinimalChart ? 'auto' : false,
							font: {
								size: plotLabelFontSize,
							},
						},
					},
				},
			};

			const image = await renderChart(configuration);
			return message.reply({
				files: [image],
			});
		} catch (e) {
			logger.error(e);
		}
	},
};

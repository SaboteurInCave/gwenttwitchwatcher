import moment from 'moment';
import 'moment-timezone';
import { getLatestTimestamp, getLeaderboards } from '../utils/functionality';
import { profileMode } from '../enums';

const Table = require('easy-table');
const Discord = require('discord.js');

module.exports = {
	'name': 'leaderboards_pro',
	'description': 'Рейтинговая таблица игроков',
	'aliases': ['leaderboardspro'],
	async execute(message, args) {

		const latestTimestamp = await getLatestTimestamp();
		const users = await getLeaderboards(profileMode.PRO, latestTimestamp);

		if (users.length > 0) {
			return message.reply(makeEmbed(users, latestTimestamp));
		}
		else {
			return message.reply('Не найдены данные!');
		}
	},
};

const makeEmbed = (data, timestamp) => {
	return new Discord.MessageEmbed()
		.setTitle(`Таблица лидеров про-ранга (${moment(timestamp).tz('Europe/Moscow').format('DD-MM-YYYY HH:mm')})`)
		.setDescription(makeLeaderboardsTable(data.map(user => user.profile)))
		.setFooter(global.config.bot_name)
		.setTimestamp();
};

const makeLeaderboardsTable = (users) => {
	let table = new Table();

	for (const [index, user] of Object.entries(users)) {
		table.cell('Номер', Number(index) + 1);
		table.cell('Имя', user.name);
		table.cell('Позиция', user.position);
		table.cell('MMR', user.mmr);
		table.newRow();
	}

	return `\`\`\`${table.toString()}\`\`\``;
};

import { getPermissions } from '../utils/permissions';

import QueueManager from '../queue/queueManager';
import logger from '../logger';
import { argsEnum } from '../enums';

const Discord = require('discord.js');

module.exports = {
	'name': 'start_queue',
	'description': 'Создание очереди бота',
	'args': argsEnum.REQUIRED,
	'usage': `${global.config.prefix}startQueue <queue_name> (только для модераторов)`,
	'aliases': ['startqueue'],
	async execute(message, args) {

		if (!getPermissions(message)) {
			return message.reply('Недостаточно прав для использования команды!'); //.then(e => e.delete(global.config.removeRate));
		}

		const queueName = args[0]; //.toLowerCase();

		const queue = await QueueManager.getQueue(queueName);

		if (queue) {
			try {
				message.reply(`Бот начинает работу очереди _${queueName}_...`);
				await QueueManager.startJobs(queue);
				message.reply(`Очередь _${queueName}_ начинает свою работу!`)
			} catch (e) {
				message.reply(`Произошла ошибка при создании очереди! ${JSON.stringify(e)}`)
			}
		} else {
			message.reply(`Очередь _${queueName}_ не найдена!`)
		}
	},
};

import { argsEnum, factionsEnum } from '../enums';
import Stats from '../db/stats';
import logger from '../logger';
import User from '../db/user';
import { getFactions, timestampHoursDifference } from '../utils';

import moment from 'moment';
import 'moment-timezone';
import { removeCustomRoles } from '../utils/roles';
import { factionLocalization } from '../utils/canvas';
import { getMinFactionRoleLimit } from '../utils/settings';

module.exports = {
	'name': 'set_faction',
	'description': 'Установка выбранной фракционной роли',
	'args': argsEnum.REQUIRED,
	'usage': `${global.config.prefix}setFaction <faction>`,
	'aliases': ['setfaction'],
	async execute(message, args) {
		const minFactionRoleLimit = await getMinFactionRoleLimit();
		const factions = Object.values(getFactions()); // [factionsEnum.NR, factionsEnum.NG, factionsEnum.SC, factionsEnum.MO, factionsEnum.SK];

		const userData = await User.findOne({ id: message.author.id });

		if (!userData) {
			return message.reply(`Не найден пользователь ${message.author.username} в системе!`);
		}

		logger.debug(userData);

		if (timestampHoursDifference(userData.factionChoice.timestamp, Date.now()) < 24) {
			logger.debug(timestampHoursDifference(userData.factionChoice.timestamp, Date.now()));
			logger.debug(userData.factionChoice.timestamp);
			logger.debug(Date.now());
			const time = moment(userData.factionChoice.timestamp).add(24, 'hours').tz('Europe/Moscow').format('DD-MM-YYYY HH:mm (Z)');
			return message.reply(`Фракция была изменена совсем недавно - обновить можно будет в ${time}.`);
		}

		let user = await Stats.find({ player: message.author.id }).sort('-timestamp').limit(1);
		if (user.length === 0) {
			return message.reply(`Не найдена статистика пользователя ${message.author.username}!`);
		}

		user = user[0];

		if (args.length === 0) {
			return message.reply(`Необходимо передать желаемую фракционную роль!`);
		}

		let faction = args[0].trim();

		try {
			if (faction === factionLocalization[factionsEnum.SC]) {
				faction = factionsEnum.SC;
			}

			if (!factions.includes(faction)) {
				return message.reply(`Введена недопустимая фракция! Возможные варианты: \`\`${Object.values(factionLocalization).join(', ')}\`\``);
			}
		} catch (e) {
			logger.error(e.stack);
		}


		const data = user.wins.total;
		let factionData = Object.assign({}, data);
		delete factionData.$init;
		delete factionData.overall;

		factionData = Object.entries(factionData).sort((a, b) => a[1] < b[1]);
		const chosenFactionEntry = factionData.find(entry => entry[0] === faction);

		if (chosenFactionEntry[1] < minFactionRoleLimit) {
			return message.reply(`Количество побед *${chosenFactionEntry[1]}* на фракции \`\`${factionLocalization[chosenFactionEntry[0]]}\`\` недостаточно для получения роли! Необходимо получить ${minFactionRoleLimit} побед!`);
		}

		const factionRoles = global.config.roles.faction;
		const role = message.guild.roles.cache.get(factionRoles[chosenFactionEntry[0]]);

		if (!role) {
			return message.reply('Не найдена подходящая роль на сервере!');
		}

		await removeCustomRoles(message.member, Object.values(global.config.roles.faction), `${message.author.username} решил сменить фракцию: ${factionLocalization[chosenFactionEntry[0]]}`);
		await message.member.roles.add(role, `${message.author.username} решил сменить фракцию: ${factionLocalization[chosenFactionEntry[0]]}`);

		userData.factionChoice.faction = chosenFactionEntry[0];
		userData.factionChoice.timestamp = Date.now();
		await userData.save();

		logger.debug(userData);

		return message.reply(`Вы выбрали роль ${role}. Она не будет снята во время обновления статистики до тех пор, пока не будет введена команда \`\`${global.config.prefix}unsetFaction\`\`. Если появилось желание изменить роль, то через сутки это станет возможным.`);
	},
};

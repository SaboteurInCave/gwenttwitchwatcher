import 'moment-timezone';
import { profileMode } from '../enums';
import Stats from '../db/stats';
import logger from '../logger';
import axios from 'axios';

const Discord = require('discord.js');

const parseDate = (dateString) => {
	const date = new Date(dateString);
	return date.getTime();
};

module.exports = {
	'name': 'leaderboards_pro_race',
	'description': 'График изменения позиции игроков в про-ранге',
	'aliases': ['leaderboardsprorace'],
	async execute(message, args) {

		// if (args.length < 4) {
		// 	return message.reply('Недостаточное количество аргументов!');
		// }
		//
		// let firstDate = null;
		// let secondDate = null;
		// try {
		// 	firstDate = parseDate(`${args[0]} ${args[1]}`);
		// 	secondDate = parseDate(`${args[2]} ${args[3]}`);
		// } catch (e) {
		// 	return message.reply('Ошибка обработки даты');
		// }
		//
		// try {
		// 	const stats = await Stats.find({
		// 		timestamp: { $gte: firstDate, $lte: secondDate },
		// 		// 'profile.position': { $gt: 0 },
		// 		'profile.mode': profileMode.PRO,
		// 	});
		//
		// 	logger.info('data acquired');
		//
		// 	const data = [
		// 		['name', 'timestamp', 'value'],
		// 	];
		//
		// 	for (const entry of stats) {
		// 		data.push(
		// 			[entry.profile.name, entry.timestamp, entry.profile.mmr],
		// 		);
		// 	}
		//
		// 	const response = await axios.request({
		//
		// 		url: `${global.config.backend_address}/api/charts/leaderboards_race`,
		// 		method: 'post',
		// 		data: { data: data },
		// 		responseType: 'arraybuffer',
		// 		// headers: {
		// 		// 	'Content-Type': 'image/gif',
		// 		// },
		// 		// timeout: 10000,
		// 	});
		//
		// 	logger.info(data);
		//
		// 	message.reply({
		// 		files: [data],
		// 	});
		//
		// } catch (e) {
		// 	logger.error(e);
		// 	return message.reply('Ошибка извлечения данных');
		// }


		// YYYY-MM-DD HH:mm:dd

		// const latestTimestamp = await getLatestTimestamp();
		// const users = await getLeaderboards(profileMode.PRO, latestTimestamp);
		//
		// if (users.length > 0) {
		// 	return message.reply(makeEmbed(users, latestTimestamp));
		// }
		// else {
		// 	return message.reply('Не найдены данные!');
		// }
	},
};

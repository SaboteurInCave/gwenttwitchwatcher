import { argsEnum } from '../enums';
import { getSettings, setSettings, settingsText } from '../utils/settings';
import { getPermissions } from '../utils/permissions';
import Settings from '../db/settings';
import logger from '../logger';

module.exports = {
	'name': 'set_settings',
	'description': 'Изменение значений внутренних параметров бота',
	'args': argsEnum.REQUIRED,
	'usage': `${global.config.prefix}setSettings [field] [value], где field:${settingsText}\nvalue - значение`,
	'aliases': ['setsettings'],
	async execute(message, args) {

		if (!getPermissions(message)) {
			return message.reply('Недостаточно прав для использования команды!');
		}

		if (args.length < 2) {
			return message.reply('Недостаточное количество аргументов для правильной работы команды!');
		}

		let field = args[0].trim();
		let value = args[1].trim();

		try {
			// cast to number
			logger.debug(Settings.schema.obj);
			if (field in Settings.schema.obj) {
				const fieldFunction = Settings.schema.obj[field].type;
				value = fieldFunction(value);

				if (value === undefined || value === null || isNaN(value)) {
					return message.reply('Неправильный формат нового значения настройки!')
				}
			}
		} catch (e) {
			logger.error(e);
			return message.reply('Что-то пошло не так во время обработки аргументов, обратиесь к администратору!')
		}


		try {
			const result = await setSettings(field, value);
			return message.reply(`Новое значение поля ${field} = ${value}`);
		} catch (e) {
			return message.reply(e);
		}
	},
};

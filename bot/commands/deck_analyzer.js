import axios from 'axios';
import logger from '../logger';

const Discord = require('discord.js');

const makeDescription = (data) => {
	let result = `${data['cards_count']} карт, ${data['total_provision']} провизии, ${data['crafting_cost']} обрывков
				Цвета:  ${data['colors']['Gold']} 🥇, ${data['colors']['Bronze']} 🥉
				Типы карт:`;

	const types = data['types'];
	if (types.hasOwnProperty('Unit')) {
		result += ` ${types['Unit']} 💂‍♂️`
	}

	if (types.hasOwnProperty('Special')) {
		result += `, ${types['Special']} ⚡`
	}

	if (types.hasOwnProperty('Artifact')) {
		result += `, ${types['Artifact']} 🏺`
	}

	result += `\nБелая сила колоды: ${data['total_raw_strength']}`;

	return result
};

const cardColors = {
	'Gold': '🥇'	,
	'Bronze': '🥉'
};

const cardTypes = {
	'Leader': '👑',
	'Stratagem' : '⚖️',
	'Unit': '‍💂‍♂️',
	'Special': '⚡',
	'Artifact': '🏺'
};

const cardData = {
	'provision': '🍞',
	'strength': '⚔️',
	'armor': '🛡️'
};

module.exports = {
	'name': 'deck_analyzer',
	'description': 'Анализ колоды с сайта playgwent.com',
	'aliases': ['deckanalyzer'],
	async execute(message, args) {

		if (args.length < 1) {
			return message.reply('Недостаточное количество аргументов!');
		}

		try {
			const response = await axios.request({

				url: `${global.config.backend_address}/api/deck_analyzer`,
				method: 'post',
				data: { link: args[0] },
			});

			const data = response.data;
			let messageEmbed = new Discord.MessageEmbed()
				.setTitle('Анализ колоды')
				.setDescription(makeDescription(data))
				.setFooter(global.config.bot_name)
				.setTimestamp();

			// leader
			const leader = data.leader;
			messageEmbed = messageEmbed.addField(
				`${cardTypes['Leader']} ${leader['name']}`,
				`${leader['provision']} ${cardData['provision']}`,
				true
			);

			// straqtegem
			const stratagem = data.stratagem;
			messageEmbed = messageEmbed.addField(
				`${cardTypes['Stratagem']} ${stratagem['name']}`,
				'Только для первого хода',
				true
			);

			// deck cards
			for (const card of data['cards']) {
				let title = `${cardTypes[card['type']]} ${cardColors[card['color']]} ${card['name']}`;
				if (card['count'] > 1) {
					title += ` (x${card['count']})`
				}

				let description = `${card['provision']} ${cardData['provision']}`;

				if (card['strength'] > 0) {
					description += ` | ${card['strength']} ${cardData['strength']}`
				}

				if (card['armor'] > 0) {
					description += ` | ${card['armor']} ${cardData['armor']}`
				}

				messageEmbed = messageEmbed.addField(title, description, true);
			}
			//message;

			messageEmbed.addField('Легенда', `🥇: золотая карта, 🥉: бронзовая карта, 👑: лидер, ⚖️: стратагема, 💂‍♂️: отряд, ⚡: особая карта, 🏺: артефакт, 🍞: провизия, ⚔️: сила отряда, 🛡: броня отряда`);

			// return message.reply(`Analysis result: ${JSON.stringify(response.data)}`);
			return message.reply(messageEmbed);
		} catch (e) {
			logger.error(e);
			return message.reply('Ошибка при анализе колоды :(');
		}
	},
};

import { argsEnum } from '../enums';
import { addGogNickname, formateDate } from '../utils';
import { isUserHidden } from '../utils/permissions';
import { getUsers } from '../utils/messages';
import fs from 'fs';
import { factionLocalization } from '../utils/canvas';
import { setPlayerImage } from '../utils/functionality';

const Table = require('easy-table');
const Discord = require('discord.js');
const path = require('path');

module.exports = {
	'name': 'get_profile_factions',
	'description': 'Получение сезонных побед за фракцию пользователя. Без указания никнейма пользователя будет выведена статистка автора сообщения.',
	'args': argsEnum.OPTIONAL,
	'usage': `${global.config.prefix}getProfileFactions или ${global.config.prefix}getProfileFactions [username]`,
	'aliases': ['getprofilefactions'],
	async execute(message, args) {
		const users = await getUsers(args.length === 0 ? message.author : args.join(' '), message.client.users.cache);
		const username = args.length === 0 ? message.author.username : args.join(' ');

		if (users.length === 0) {
			let text = `Не найден пользователь ${username}!`;

			if (args.length === 0) {
				text += ` Для того, чтобы привязать свой профиль к боту, необходимо выполнить команду !setProfile *gog_nickname*.`;
			}

			return message.reply(text);
		}

		for (const user of users) {
			const isHidden = await isUserHidden(user.player);

			if (isHidden) {
				let text = `Статистика ${username} скрыта!`;
				if (args.length === 0) {
					text += ` Необходимо открыть свой профиль на сайте playgwent.com для дальнейшего отслеживания.`;
				}
				message.reply(text);
				continue;
			}

			message.reply(await makeEmbed(user, args.length === 0 ?
				message.author.username :
				message.client.users.cache.find(discordUser => discordUser.id === user.player).username,
				),
			);
		}
	},
};

const makeEmbed = async (data, nickname) => {
	const message = new Discord.MessageEmbed()
		.setTitle(`Фракционные игры ${nickname}${addGogNickname(nickname, data.profile.name)}`)
		//.setThumbnail(`https://cdn-l-playgwent.cdprojektred.com/avatars/${data.profile.avatar}.jpg`)
		.setURL(`https://www.playgwent.com/ru/profile/${data.profile.name}`)
		.setDescription(`Данные от *${formateDate(data.timestamp)}*`)
		.addField(`Фракции в сезоне:`, makeFactionTable(data.season.factions))
		// .addField(`Расширенная статистика`, `https://stats.gwentlib.com/season/${data.profile.name}`)
		.setFooter(global.config.bot_name)
		.setTimestamp();

	await setPlayerImage(message, data.profile);

	return message;
};

const makeFactionTable = (data) => {
	let table = new Table();
	let factionData = Object.assign({}, data);
	delete factionData.$init;

	factionData = Object.entries(factionData).sort(((a, b) => {
		const mmrDiff = b[1].mmr - a[1].mmr;
		return mmrDiff !== 0 ? mmrDiff : b[1].games - a[1].games;
	}));

	for (const faction of factionData) {
		table.cell('Фракция', factionLocalization[faction[0]]);
		table.cell('fMMR', faction[1].mmr);
		table.cell('Игры', faction[1].games);
		table.newRow();
	}

	return `\`\`\`${table.toString()}\`\`\``;
};

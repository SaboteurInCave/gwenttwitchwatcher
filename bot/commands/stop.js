import { getPermissions } from '../utils/permissions';
import QueueManager from '../queue/queueManager';

module.exports = {
	'name': 'stop_stream_watching',
	'description': 'Stop stream watching in the current room',
	async execute(message) {
		if (!getPermissions(message)) {
			return message.reply('Нет достаточных прав для использования команды!') //.then(e => e.delete(global.config.removeRate));
		}

		await QueueManager.stop();
		return message.reply('Слежение за стримами остановлено! Выполняются последние задания...') //.then(e => e.delete(global.config.removeRate))
	},
};

import { argsEnum } from '../enums';
import { getPermissions } from '../utils/permissions';
import { getSettings, settingsText } from '../utils/settings';

module.exports = {
	'name': 'get_settings',
	'description': 'Получение значений внутренних параметров бота',
	'args': argsEnum.OPTIONAL,
	'usage': `${global.config.prefix}getSettings [field], где field:${settingsText}`,
	'aliases': ['getsettings'],
	async execute(message, args) {

		if (!getPermissions(message)) {
			return message.reply('Недостаточно прав для использования команды!');
		}

		if (args.length > 0) {
			const field = args[0].trim();

			try {
				const result = await getSettings(field);
				return message.reply(`Значение: ${result}`);
			} catch (e) {
				return message.reply(e);
			}
		} else {
			try {
				const result = await getSettings(null);
				return message.reply(`\nДоступные параметры:\n${result}`);
			} catch (e) {
				return message.reply(e);
			}
		}


	},
};

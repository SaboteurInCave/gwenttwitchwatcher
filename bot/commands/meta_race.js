import 'moment-timezone';
import logger from '../logger';
import axios from 'axios';


const parseDate = (dateString) => {
	const date = new Date(dateString);
	return date.getTime();
};

module.exports = {
	'name': 'meta_race',
	'description': 'График изменения меты в про-ранге',
	'aliases': ['metarace'],
	async execute(message, args) {

		// if (args.length < 2) {
		// 	return message.reply('Недостаточное количество аргументов!');
		// }
		//
		// let firstDate = null;
		// let secondDate = null;
		// try {
		// 	firstDate = parseDate(`${args[0]} ${args[1]}`);
		// 	secondDate = parseDate(`${args[2]} ${args[3]}`);
		// } catch (e) {
		// 	return message.reply('Ошибка обработки даты');
		// }
		//
		// try {
		// 	const response = await axios.request({
		//
		// 		url: `${global.config.backend_address}/api/charts/meta_race`,
		// 		method: 'post',
		// 		data: { start: firstDate, finish: secondDate },
		// 	});
		//
		// 	return message.reply("Video was created")
		// } catch (e) {
		// 	logger.error(e);
		// 	return message.reply('Ошибка извлечения данных');
		// }
	},
};

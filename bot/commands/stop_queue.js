import { getPermissions } from '../utils/permissions';

import QueueManager from '../queue/queueManager';
import logger from '../logger';
import { argsEnum } from '../enums';

const Discord = require('discord.js');

module.exports = {
	'name': 'stop_queue',
	'description': 'Остановка работы одной из очередей бота',
	'args': argsEnum.REQUIRED,
	'usage': `${global.config.prefix}stopQueue <queue_name> (только для модераторов)`,
	'aliases': ['stopqueue'],
	async execute(message, args) {

		if (!getPermissions(message)) {
			return message.reply('Недостаточно прав для использования команды!'); //.then(e => e.delete(global.config.removeRate));
		}

		const queueName = args[0]; //.toLowerCase();

		const queue = await QueueManager.getQueue(queueName);

		if (queue) {
			try {
				message.reply(`Бот останавливает работу очереди _${queueName}_...`);
				await QueueManager.stopJobs(queue);
				message.reply(`Работа очереди _${queueName}_ остановлена!`)
			} catch (e) {
				message.reply(`Произошла ошибка при остановке очереди! ${JSON.stringify(e)}`)
			}
		} else {
			message.reply(`Очередь _${queueName}_ не найдена!`)
		}
	},
};

import Stats from '../db/stats';
import Users from '../db/user';

import DiscordWatcher from '../utils/discord';
import QueueManager from '../queue/queueManager';
import moment from 'moment';
import 'moment-timezone';
import logger from '../logger';


const Discord = require('discord.js');
const Table = require('easy-table');

module.exports = {
	'name': 'watcher_stats',
	'description': 'Внеочередное обновление статистики (только для модераторов!)',
	'aliases': ['watcherstats'],
	async execute(message) {
		try {
			const latestTimestamp = (await Stats.find().distinct('timestamp')).slice(-1)[0];
			if (latestTimestamp !== null) {
				const users = await Stats.find({ timestamp: latestTimestamp });
				const allUsers = await Users.find({});
				const data = await getEmbed(allUsers.length, users);
				return message.reply(data); //.then(e => e.delete(global.config.removeRate));
			}
			else {
				return message.reply('Не найдены данные!'); //.then(e => e.delete(global.config.removeRate));
			}
		} catch (e) {
			logger.error(e)
		}
	},
};

const getEmbed = async (overall, seasonalUsers) => {
	// FIXME: seasonalUsers - fix users with zero games
	seasonalUsers = seasonalUsers.filter(user => user.season.overall.games > 0);
	const seasonGames = seasonalUsers.reduce((a, b) => a + (b.wins.season.overall || 0), 0);

	const guild = DiscordWatcher.client.guilds.cache.first();
	const members = await guild.members.fetch();

	const factionRolesRaw = global.config.roles.faction;
	let factionSums = {};
	const factionRoles = Object.values(factionRolesRaw);

	for (const member of members.array()) {
		const role = member.roles.cache.find(r => factionRoles.includes(r.id));

		if (role) {
			if (!(role in factionSums)) {
				factionSums[role] = 0;
			}

			factionSums[role] += 1;
		}
	}

	factionSums = Object.entries(factionSums).sort((a, b) => b[1] - a[1]);

	const queue = QueueManager.getQueue('gwent');
	const job = (await queue.getRepeatableJobs())[0] || null;

	let message = new Discord.MessageEmbed()
		.setTitle(`Cтатистика ${global.config.bot_name}`)
		.setFooter(global.config.bot_name)
		.setTimestamp();

	let description = `В базе игроков находятся **${overall}** игроков.\n
		Суммарно за сезон **${seasonalUsers.length}** игроков сыграли **${seasonGames}** игр.\n
		Разделение людей по фракциям:\n${factionSums.map(([r, value]) => `* ${r} - ${value}\n`).join(' ')}\n`;

	if (job !== null) {
		description += `Следующее плановое обновление будет в ${moment(job.next).tz('Europe/Moscow').format('DD-MM-YYYY HH:mm')}`;
	}

	message = message.setDescription(description);

	return message;
};

// const getTable = (data) => {
// 	let table = new Table();
//
// 	for (const [faction, sum] of Object.entries(data)) {
// 		table.cell('Фракция', faction);
// 		table.cell('Количество', sum);
// 		table.newRow();
// 	}
//
// 	return `\`\`\`${table.toString()}\`\`\``;
// };

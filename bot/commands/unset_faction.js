import { factionsEnum } from '../enums';
import User from '../db/user';
import { removeCustomRoles } from '../utils/roles';
import { getFactions } from '../utils';
import { getMinFactionRoleLimit } from '../utils/settings';

module.exports = {
	'name': 'unset_faction',
	'description': 'Снятие с себя установленной фракционной роли',
	'usage': `${global.config.prefix}unsetFaction`,
	'aliases': ['unsetfaction'],
	async execute(message) {
		const minFactionRoleLimit = await getMinFactionRoleLimit();
		const userData = await User.findOne({ id: message.author.id });

		if (!userData) {
			return message.reply(`Не найден пользователь ${message.author.username} в системе!`);
		}

		if (!userData.factionChoice.faction === factionsEnum.NONE) {
			return message.reply(`Не найдено фракционных ролей - снимать нечего!`);
		}

		try {
			await removeCustomRoles(message.member, Object.values(global.config.roles.faction), `${message.author.username} решил снять с себя установленную фракционную роль`);

			userData.factionChoice.faction = factionsEnum.NONE;
			userData.factionChoice.timestamp = Date.now();
			await userData.save();

			return message.reply(`Установленная фракционная роль успешно снята! Во время обновления статистики будет установлена роль с максимальным количеством побед и количеством, которое >= ${minFactionRoleLimit}. Если появилось желание заново установить роль, то через сутки это станет возможным.`);

		} catch (e) {
			return message.reply('Не удалось снять фракционную роль - обратитесь к модератороам за помощью!')
		}

	},
};

import { addGogNickname, formateDate } from '../utils';
import fs from 'fs';
import { argsEnum } from '../enums';
import logger from '../logger';
import { isUserHidden } from '../utils/permissions';
import { getUsers } from '../utils/messages';
import { setPlayerImage } from '../utils/functionality';

const Table = require('easy-table');

const Discord = require('discord.js');
const path = require('path');


const makeGamesTable = (data) => {
	const table = new Table();

	table.cell('Результат', 'Победа');
	table.cell('Количество', data.games.wins);
	table.newRow();

	table.cell('Результат', 'Поражение');
	table.cell('Количество', data.games.loses);
	table.newRow();

	table.cell('Результат', 'Ничья');
	table.cell('Количество', data.games.ties);
	table.newRow();

	return `\`\`\`${data.overall.games} матчей, ${(data.games.winrate * 100).toFixed(2)} % winrate\n\n${table.toString()} \`\`\``;
};

const makeEmbed = async (data, nickname) => {
	const message = new Discord.MessageEmbed()
		.setTitle(`Общая статистика ${nickname}${addGogNickname(nickname, data.profile.name)}`)
		//.setThumbnail(`https://cdn-l-playgwent.cdprojektred.com/avatars/${data.profile.avatar}.jpg`)
		.setDescription(`${data.profile.rank} ранг, ${data.profile.level} уровень (престиж: ${data.profile.prestige}), ${data.profile.mmr} mmr, позиция: ${data.profile.position}\n\nДанные от *${formateDate(data.timestamp)}*`)
		.addField(`Игры в сезоне`, makeGamesTable({ overall: data.season.overall, games: data.season.games }))
		// .addField(`Расширенная статистика`, `https://stats.gwentlib.com/season/${data.profile.name}`)
		.setURL(`https://www.playgwent.com/ru/profile/${data.profile.name}`)
		.setFooter(`${global.config.bot_name}`)
		.setTimestamp();

	await setPlayerImage(message, data.profile);
	return message;
};

module.exports = {
	'name': 'get_profile',
	'description': 'Получение профиля пользователя. Без указания никнейма пользователя будет выведена статистка автора сообщения.',
	'args': argsEnum.OPTIONAL,
	'usage': `${global.config.prefix}getProfile или ${global.config.prefix}getProfile [username]`,
	'aliases': ['getprofile'],
	async execute(message, args) {
		try {
			const users = await getUsers(args.length === 0 ? message.author : args.join(' '), message.client.users.cache);
			const username = args.length === 0 ? message.author.username : args.join(' ');

			if (users.length === 0) {
				let text = `Не найден пользователь ${username}!`;

				if (args.length === 0) {
					text += ` Для того, чтобы привязать свой профиль к боту, необходимо выполнить команду !setProfile *gog_nickname*.`
				}

				return message.reply(text);
			}

			for (const user of users) {
				logger.info(user);
				const isHidden = await isUserHidden(user.player);

				if (isHidden) {
					let text = `Статистика ${username} скрыта!`;
					if (args.length === 0) {
						text += ` Необходимо открыть свой профиль на сайте playgwent.com для дальнейшего отслеживания.`
					}
					message.reply(text);
					continue;
				}

				message.reply(await makeEmbed(user, args.length === 0 ?
					message.author.username :
					message.client.users.cache.find(discordUser => discordUser.id === user.player).username,
					),
				);
			}
		} catch (e) {
			logger.error(e);
		}
	},
};

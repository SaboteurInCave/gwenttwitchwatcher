import { argsEnum } from '../enums';
import Stats from '../db/stats';
import User from '../db/user';
import { getPermissions } from '../utils/permissions';
import logger from '../logger';
import { getRoles, removeCustomRoles } from '../utils/roles';

let mongoose = require('mongoose');


module.exports = {
	'name': 'unset_profile',
	'description': 'Удаление профиля из системы',
	'args': argsEnum.REQUIRED,
	'usage': `${global.config.prefix}unsetProfile <discord_username>`,
	'aliases': ['unsetprofile'],
	async execute(message, args) {
		const username = args.join(' ').toLowerCase();

		try {
			if (!getPermissions(message)) {
				return message.reply('Недостаточно прав для использования команды!'); //.then(e => e.delete(global.config.removeRate));
			}

			const guild = message.guild;
			const members = await guild.members.fetch();
			const userMember = members.array().find(entry => entry.user.username.toLowerCase() === username);

			if (!userMember) {
				return message.reply(`Не найден пользователь ${username}!`); //.then(e => e.delete(global.config.removeRate));
			}

			await User.findOneAndDelete({ id: userMember.user.id });
			await await Stats.deleteMany({ player: userMember.user.id });

			await removeCustomRoles(userMember, getRoles(), `С пользователя ${userMember.user.username} был снят профиль`);

			message.reply(`Записи о пользователе ${userMember.user.username} удалены!`); //.then(embed => embed.delete(global.config.removeRate));
		} catch (e) {
			logger.error(e);
			logger.error(e.stack);
			return message.reply(`Не удалось удалить записи о пользователе ${username}. Обратитесь к администраторам за подробностями...`);
		}
	},
};

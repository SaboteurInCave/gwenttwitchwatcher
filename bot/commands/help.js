module.exports = {
	name: 'watcher_help',
	description: 'Информации',
	aliases: ['watcherhelp'],
	async execute(message, args) {
		const data = [];
		const { commands } = message.client;

		if (!args.length) {
			data.push(`Лист доступных команд для бота ${global.config.bot_name}\n`);
			data.push(`\`\`\`${commands.map(command => '* ' + command.name).join('\n')}\`\`\``);
			data.push(`\nДля более детальной информации о команде введите \`${global.config.prefix}watcherHelp command_name\``);

			return message.channel.send(data, { split: true }) //.then(m => m.delete(global.config.removeRate));
		}


		const name = args[0].toLowerCase();
		const command = commands.get(name) || commands.find(c => c.aliases && c.aliases.includes(name));

		if (!command) {
			return message.reply('Такой команды не существует!');
		}

		data.push(`**Название:** ${command.name}`);

		if (command.aliases) data.push(`**Псевдонимы:** ${command.aliases.join(', ')}`);
		if (command.description) data.push(`**Описание:** ${command.description}`);
		if (command.usage) data.push(`**Использование:** ${command.usage}`);

		await message.channel.send(data, { split: true }) //.then(m => m.delete(global.config.removeRate));

	},
};

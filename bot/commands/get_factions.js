import Stats from '../db/stats';
import { addGogNickname, formateDate } from '../utils';
import logger from '../logger';
import { getMinFactionRoleLimit } from '../utils/settings';

const Discord = require('discord.js');


module.exports = {
	'name': 'get_factions',
	'description': 'Получение списка фракционных ролей, доступных для пользователя',
	'usage': `${global.config.prefix}getFactions`,
	'aliases': ['getfactions'],
	async execute(message) {
		const minFactionRoleLimit = await getMinFactionRoleLimit();
		let user = await Stats.find({ player: message.author.id }).sort('-timestamp').limit(1);

		if (user.length === 0) {
			return message.reply(`Не найден пользователь ${message.author.username}!`);
		}

		user = user[0];

		const data = user.wins.total;
		let factionData = Object.assign({}, data);
		delete factionData.$init;
		delete factionData.overall;

		factionData = Object.entries(factionData).filter(item => item[1] >= minFactionRoleLimit).sort((a, b) => a[1] < b[1]);
		try {
			return message.reply(makeEmbed(user, message.client, message.author.username, factionData));
		} catch (e) {
			logger.error(e)
		}

	},
};

const makeEmbed = (data, client, nickname, factions) => {
	const factionRoles = global.config.roles.faction;

	const message = new Discord.MessageEmbed()
		.setTitle(`Фракционные роли ${nickname}${addGogNickname(nickname, data.profile.name)}`)
		.setDescription(`Данные от *${formateDate(data.timestamp)}*`)
		.setFooter(global.config.bot_name)
		.setTimestamp();

	for (const [index, factionEntry] of Object.entries(factions)) {
		const role = client.guilds.cache.first().roles.cache.get(factionRoles[factionEntry[0]]);
		message.addField(`Фракция #${Number(index) + 1}`, `${role} (Количество побед: ${factionEntry[1]})\nДля получения фракционной роли вбейте команду: \`\`!setFaction ${factionEntry[0]}\`\``);
	}

	return message;
};

import { getPermissions } from '../utils/permissions';
import QueueManager from '../queue/queueManager';

module.exports = {
	'name': 'force_update',
	'description': 'Внеочередное обновление статистики (только для модераторов!)',
	'aliases': ['forceupdate'],
	async execute(message) {
		if (!getPermissions(message)) {
			return message.reply('Недостаточно прав для использования команды!');
		}

		QueueManager.startGwentTask();
		return message.reply('Началось внеочередное обновление статистики! Ожидайте результатов в ближайшее время!'); //.then(e => e.delete(global.config.removeRate))
	},
};

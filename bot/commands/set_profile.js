import { getPermissions } from '../utils/permissions';

const Discord = require('discord.js');

import User from '../db/user';
import { argsEnum, factionsEnum } from '../enums';
import axios from 'axios';
import logger from '../logger';

const cheerio = require('cheerio');

import axiosRetry from 'axios-retry';
import QueueManager from '../queue/queueManager';

import moment from 'moment';
import 'moment-timezone';

axiosRetry(axios, { retries: 5, retryDelay: axiosRetry.exponentialDelay });

module.exports = {
	'name': 'set_profile',
	'description': 'Привязка GOG и Discord профилей. На основе GOG профиля происходит работа с профилем игрока в Гвинт - без данной команды отслеживание статистики не будет возможным!',
	'args': argsEnum.REQUIRED,
	'usage': `${global.config.prefix}setProfile <GOG_profile> или ${global.config.prefix}setProfile <username> <GOG_profile> (только для модераторов)`,
	'aliases': ['setprofile'],
	async execute(message, args) {
		let gogUser = null; // only text field
		let discordUser = null; // object is possible

		logger.debug(args);

		if (args.length === 1) {
			discordUser = message.author;
			gogUser = args[0].toLowerCase();

			logger.debug(gogUser);
			logger.debug(discordUser.username);
		}
		else {
			if (!getPermissions(message)) {
				return message.reply('Недостаточно прав для использования команды!'); //.then(e => e.delete(global.config.removeRate));
			}

			gogUser = args.pop().toLowerCase();
			discordUser = args.join(' ');

			logger.debug(gogUser);
			logger.debug(discordUser.toLowerCase());
		}

		// check if user already has a account in system
		const accountDuplicate = await User.findOne({ id: message.author.id });

		if (accountDuplicate) {
			return message.reply(`Ваш профиль уже есть в системе! Gog-профиль: ${accountDuplicate.gog}`);
		}

		// find is gog account already used
		const gogDuplicate = await User.findOne({ gog: gogUser });

		if (gogDuplicate) {
			const duplicateUser = message.client.users.cache.find(user => {
				return user.id === gogDuplicate.id;
			});
			return message.reply(`Gog-профиль ${gogUser} уже занят пользователем ${duplicateUser.username}. Если это был Ваш профиль, то обратитесь к модераторам`);
		}

		if (typeof discordUser == 'string') {
			// find user in the discord
			discordUser = message.client.users.cache.find(user => {
				return user.username.toLowerCase() === discordUser.toLowerCase();
			});
		}

		if (discordUser === null) {
			return message.reply(`Не найден пользователь!`); //.then(e => e.delete(global.config.removeRate));
		}

		try {

			logger.debug(`https://www.playgwent.com/ru/profile/${gogUser}`);

			const result = await axios.get(`https://www.playgwent.com/ru/profile/${gogUser}`, {
				headers: {
					'Content-Length': 0,
					'Content-Type': 'text/plain',
				},
				responseType: 'text',
			});

			const data = result.data;

			const dom = cheerio.load(result.data);
			const text = dom.text();

			if (text.includes('ПРОФИЛЬ ИГРОКА СКРЫТ')) {
				return message.reply(`Профиль пользователя ${gogUser} **скрыт** - невозможно вести статистику!`); //.then(e => e.delete(global.config.removeRate));
			}

			logger.debug(dom.text());

			// add user to database!
			const user = new User({ id: discordUser.id, gog: gogUser });
			await user.save();

			const queue = QueueManager.getQueue('gwent');
			const job = (await queue.getRepeatableJobs())[0] || null;

			let description = `GOG профиль ${gogUser} добавлен пользователю ${discordUser.username}! `;

			if (job !== null) {
				description += `Данные можно будет получить в: \`${moment(job.next).tz('Europe/Moscow').format('DD-MM-YYYY HH:mm (Z)')}\``;
			}

			return message.reply(description); //.then(e => e.delete(global.config.removeRate));

		} catch (e) {
			// 404
			logger.error(e);
			return message.reply(`GOG-профиль пользователя ${gogUser} не найден!`); //.then(e => e.delete(global.config.removeRate));
		}
	},
};

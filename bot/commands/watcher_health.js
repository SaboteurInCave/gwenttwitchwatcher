import { getPermissions } from '../utils/permissions';

import QueueManager from '../queue/queueManager';
import DicePokerManager from '../games/dicePoker/diceManager'

import logger from '../logger';
import { inspect } from 'util'

const Discord = require('discord.js');

module.exports = {
	'name': 'watcher_health',
	'description': 'Осмотр бота и его очередей',
	'aliases': ['watcherhealth'],
	async execute(message) {

		if (!getPermissions(message)) {
			return message.reply('Недостаточно прав для использования команды!'); //.then(e => e.delete(global.config.removeRate));
		}

		const data = await QueueManager.getJobs();
		// const localization = DicePokerManager.localization;
		// const dices = DicePokerManager.getHand();

		// return message.reply(`Кости: ${DicePokerManager.getDiceRepresentation(dices)}, комбинация: ${localization.combinations[DicePokerManager.getDiceCombination(dices)]}` );
		message.reply(makeEmbed(data));


		// const latestTimestamp = (await Stats.find().distinct('timestamp')).slice(-1)[0];
		// if (latestTimestamp !== null) {
		// 	const users = await Stats.find({ timestamp: latestTimestamp });
		// 	const allUsers = await Users.find({});
		// 	const data = await getEmbed(allUsers.length, users);
		// 	return message.reply(data) //.then(e => e.delete(global.config.removeRate));
		// }
		// else {
		// 	return message.reply('Не найдены данные!') //.then(e => e.delete(global.config.removeRate));
		// }

	},
};

const makeEmbed = (data, timestmp) => {

	let description = '';

	for (const entry of Object.values(global.config.watchers)) {
		description += `Очередь **${entry.queue_name}**: ${entry.hasOwnProperty('description') ? entry.description : 'Неизвестно :('}\n`
	}

	description = description.replace(/\n$/, "");

	const message = new Discord.MessageEmbed()
		.setTitle(`Здоровье бота`)
		.setDescription(description)
		.setFooter(global.config.bot_name)
		.setTimestamp();

	try {
		for (const queueData of Object.values(data)) {
			message.addField(`Задачи "${queueData.queue.name}"`, `${JSON.stringify(queueData.jobs)}`);
		}
	} catch (e) {
		logger.error(e)
	}


	return message;
};

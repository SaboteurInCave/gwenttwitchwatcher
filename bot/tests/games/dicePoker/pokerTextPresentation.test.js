import { PokerTextPresentation } from '../../../games/dicePoker/pokerTextPresentation';

test('poker presentation', () => {
	const presenter = new PokerTextPresentation()

	expect(presenter.getDiceRepresentation(
		[1, 2, 3, 4, 5]
	)).toBe(
		presenter.dices.slice(0, 5).join(presenter.separator).trim()
	)
})

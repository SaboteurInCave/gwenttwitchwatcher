import DicePokerManager from '../../../games/dicePoker/diceManager';
import { GameState, HandCombinations } from '../../../games/dicePoker/enums';

test('hand size', () => {
	expect(DicePokerManager.getHand().length).toBe(DicePokerManager.groupSize);
	expect(DicePokerManager.getHand(1).length).toBe(1);
});

describe('dice hand reroll', () => {
	let dices = [1, 1, 1, 1, 1];

	test('partial reroll', () => {
		expect(DicePokerManager.modifyHand(
			dices, [0, 2, 4],
		)).toEqual(expect.arrayContaining([1, 1]));
	});


	test('ignoring incorrect indexes', () => {
		expect(DicePokerManager.modifyHand(
			dices, [null, -1, 8],
		)).toEqual(expect.arrayContaining(dices));
	});
});

describe('dice side occurrence', () => {
	const combination = [0, 2, 0, 0, 1, 2];

	test('one pair', () => {
		expect(DicePokerManager._getDiceSideByOccurrence(
			[1, 2, 0, 1, 0, 1]
		)).toStrictEqual({
			2: [1],
			1: [5, 3, 0]
		})
	})

	test('two pairs', () => {
		expect(DicePokerManager._getDiceSideByOccurrence(
			[0, 2, 0, 0, 1, 2]
		)).toStrictEqual({
			2: [5, 1],
			1: [4]
		})
	})

	test('full house', () => {
		expect(DicePokerManager._getDiceSideByOccurrence(
			[0, 2, 0, 0, 3, 0]
		)).toStrictEqual({
			3: [4],
			2: [1]
		})
	})
});

test('invalid poker combinations', () => {
	expect(DicePokerManager.getHandCombination(
		[0, 0, 7, 0, null],
	)).toBe(
		HandCombinations.NONE,
	);
});

describe('test valid poker combinations', () => {
	// pairs
	test('pairs', () => {
		expect(DicePokerManager.getHandCombination(
			[1, 1, 2, 3, 4],
		)).toBe(HandCombinations.PAIR);

		expect(DicePokerManager.getHandCombination(
			[1, 2, 6, 3, 6],
		)).toBe(HandCombinations.PAIR);
	});


	// two pairs
	test('two pairs', () => {
		expect(DicePokerManager.getHandCombination(
			[6, 4, 6, 1, 4],
		)).toBe(HandCombinations.TWO_PAIRS);

		expect(DicePokerManager.getHandCombination(
			[5, 5, 2, 2, 1],
		)).toBe(HandCombinations.TWO_PAIRS);
	});


	// three of kind
	test('three of kind', () => {
		expect(DicePokerManager.getHandCombination(
			[3, 1, 3, 2, 3],
		)).toBe(HandCombinations.THREE_KIND);
	});

	// four of kind
	test('four of kind', () => {
		expect(DicePokerManager.getHandCombination(
			[4, 2, 4, 4, 4],
		)).toBe(HandCombinations.FOUR_KIND);
	});


	// five of kind
	test('five of kind', () => {
		expect(DicePokerManager.getHandCombination(
			[4, 4, 4, 4, 4],
		)).toBe(HandCombinations.FIVE_KIND);
	});


	// five high straight
	test('five high straight', () => {
		expect(DicePokerManager.getHandCombination(
			[5, 2, 1, 4, 3],
		)).toBe(HandCombinations.FIVE_HIGH_STRAIGHT);
	});

	// six high straight
	test('six high straight', () => {
		expect(DicePokerManager.getHandCombination(
			[5, 2, 6, 4, 3],
		)).toBe(HandCombinations.SIX_HIGH_STRAIGHT);
	});


	// full house
	test('full house', () => {
		expect(DicePokerManager.getHandCombination(
			[1, 2, 1, 2, 1],
		)).toBe(HandCombinations.FULL_HOUSE);
	});

	// none
	test('none', () => {
		expect(DicePokerManager.getHandCombination(
			[1, 6, 2, 4, 5],
		));
	});

});

describe('hands evaluation', () => {
	test('tie', () => {
		expect(DicePokerManager.evaluateHands(
			[1, 1, 2, 3, 4], [1, 1, 2, 3, 4]
		)).toBe(GameState.TIE)
	})

	test('tie: both players has None', () => {
		expect(DicePokerManager.evaluateHands(
			[1, 2, 4, 6, 5], [2, 3, 4, 6, 1]
		)).toBe(GameState.TIE)
	})

	test('first pair, second none', () => {
		expect(DicePokerManager.evaluateHands(
			[1, 1, 2, 3, 4], [1, 6, 2, 3, 4]
		)).toBe(GameState.FIRST_WIN)
	})

	test('first none, second two pairs', () => {
		expect(DicePokerManager.evaluateHands(
			 [1, 6, 2, 3, 4], [1, 1, 2, 2, 4]
		)).toBe(GameState.SECOND_WIN)
	})

	test('first two pairs, second pair', () => {
		expect(DicePokerManager.evaluateHands(
			 [1, 1, 2, 2, 4], [1, 1, 2, 3, 4]
		)).toBe(GameState.FIRST_WIN)
	})

	test('first two pairs, second three kind', () => {
		expect(DicePokerManager.evaluateHands(
			 [1, 1, 2, 2, 4], [1, 1, 1, 3, 4]
		)).toBe(GameState.SECOND_WIN)
	})

	test('first five high straight, second three kind', () => {
		expect(DicePokerManager.evaluateHands(
			 [1, 2, 3, 4, 5], [1, 1, 1, 3, 4]
		)).toBe(GameState.FIRST_WIN)
	})

	test('first five high straight, second six high straight', () => {
		expect(DicePokerManager.evaluateHands(
			 [1, 2, 3, 4, 5], [2, 3, 4, 5, 6]
		)).toBe(GameState.SECOND_WIN)
	})

	test('first full house, second six high straight', () => {
		expect(DicePokerManager.evaluateHands(
			 [1, 1, 1, 2, 2], [2, 3, 4, 5, 6]
		)).toBe(GameState.FIRST_WIN)
	})

	test('first full house, second four kind', () => {
		expect(DicePokerManager.evaluateHands(
			 [1, 1, 1, 2, 2], [1, 1, 1, 1, 6]
		)).toBe(GameState.SECOND_WIN)
	})

	test('first five kind, second four kind', () => {
		expect(DicePokerManager.evaluateHands(
			 [1, 1, 1, 1, 1], [1, 1, 1, 1, 6]
		)).toBe(GameState.FIRST_WIN)
	})

	test('five kind, second win', () => {
		expect(DicePokerManager.evaluateHands(
			 [1, 1, 1, 1, 1], [6, 6, 6, 6, 6]
		)).toBe(GameState.SECOND_WIN)
	})

	test('four kind, first win', () => {
		expect(DicePokerManager.evaluateHands(
			 [2, 2, 2, 2, 6], [1, 1, 1, 1, 2]
		)).toBe(GameState.FIRST_WIN)
	})

	test('four kind, first win -> extra dice', () => {
		expect(DicePokerManager.evaluateHands(
			 [1, 1, 1, 1, 6], [1, 1, 1, 1, 2]
		)).toBe(GameState.FIRST_WIN)
	})

	test('full house, first win by first third', () => {
		expect(DicePokerManager.evaluateHands(
			 [3, 3, 3, 2, 2], [1, 1, 1, 6, 6]
		)).toBe(GameState.FIRST_WIN)
	})

	test('full house, second win by pair', () => {
		expect(DicePokerManager.evaluateHands(
			 [1, 1, 1, 2, 2], [1, 1, 1, 3, 3]
		)).toBe(GameState.SECOND_WIN)
	})
})


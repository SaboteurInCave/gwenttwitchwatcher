import { HandCombinations } from './enums';

export const textCombinations = {
	[HandCombinations.NONE]: 'Ничего',
	[HandCombinations.PAIR]: 'Пара',
	[HandCombinations.TWO_PAIRS]: 'Две пары',
	[HandCombinations.THREE_KIND]: 'Сет',
	[HandCombinations.FIVE_HIGH_STRAIGHT]: 'Малый стрейт',
	[HandCombinations.SIX_HIGH_STRAIGHT]: 'Большой стрейт',
	[HandCombinations.FULL_HOUSE]: 'Фул-хаус',
	[HandCombinations.FOUR_KIND]: 'Каре',
	[HandCombinations.FIVE_KIND]: 'Покер',
};

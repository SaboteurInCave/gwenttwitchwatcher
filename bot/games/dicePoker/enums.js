export const HandCombinations = Object.freeze({
	NONE: 0,
	PAIR: 1,
	TWO_PAIRS: 2,
	THREE_KIND: 3,
	FIVE_HIGH_STRAIGHT: 4,
	SIX_HIGH_STRAIGHT: 5,
	FULL_HOUSE: 6,
	FOUR_KIND: 7,
	FIVE_KIND: 8,
});

export const GameState = Object.freeze({
	FIRST_WIN: 'first_win',
	SECOND_WIN: 'second_win',
	TIE: 'tie'
})

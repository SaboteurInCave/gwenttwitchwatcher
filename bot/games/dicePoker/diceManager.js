import { GameState, HandCombinations } from './enums';

const zip = (a, b) => a.map((k, i) => [k, b[i]]);

class DiceManager {
	constructor({ min = 1, max = 6, groupSize = 5 } = {}) {
		this.min = min;
		this.max = max;
		this.groupSize = groupSize;
	}

	_randomDice() {
		/* Throw a dice in range [this.min, this.max] */
		return Math.floor(Math.random() * (this.max - this.min + 1)) + this.min;
	}

	getHand(size = null) {
		/* Roll group of dices */
		if (!size) {
			size = this.groupSize;
		}

		return Array.from({ length: size }, () => this._randomDice());
	}

	modifyHand(dices = [], rerolledIndexList = []) {
		const newDices = [...dices];

		for (const rerollIndex of rerolledIndexList) {
			const index = rerollIndex - 1;
			if (rerollIndex >= this.min - 1 && rerollIndex <= this.max - 1) {
				newDices[rerollIndex] = this._randomDice();
			}
		}

		return newDices;
	}

	_getHandCounter(dices = []) {
		const handCounter = new Array(this.max).fill(0);
		for (const dice of dices) {
			handCounter[dice - 1]++;
		}

		return handCounter;
	}

	_getDiceSideByOccurrence(handCounter = []) {
		/* DESC ordering! */
		const ordering = handCounter.reduce((previousValue, currentValue, index) => {
			if (currentValue > 0) {
				if (!(currentValue in previousValue)) {
					previousValue[currentValue] = [];
				}

				previousValue[currentValue].push(index);
			}
			return previousValue;
		}, {});

		for (const key of Object.keys(ordering)) {
			ordering[key].sort().reverse();
		}

		return ordering;
	}

	getHandCombination(dices = []) {
		const handCounter = this._getHandCounter(dices);

		const max = Math.max(...handCounter);

		// check 5, 4
		if (max === 5) {
			return HandCombinations.FIVE_KIND;
		}
		else if (max === 4) {
			return HandCombinations.FOUR_KIND;
		}

		// check straights

		// 0 1 2 3 4
		if (handCounter[0] === 1 && handCounter[0] === handCounter[1] && handCounter[1] === handCounter[2]
			&& handCounter[2] === handCounter[3] && handCounter[3] === handCounter[4]) {
			return HandCombinations.FIVE_HIGH_STRAIGHT;
		}

		// 1 2 3 4 5
		if (handCounter[1] === 1 && handCounter[1] === handCounter[2] && handCounter[2] === handCounter[3] &&
			handCounter[3] === handCounter[4] && handCounter[4] === handCounter[5]) {
			return HandCombinations.SIX_HIGH_STRAIGHT;
		}

		// check full house and 3 equal dices in group

		if (max === 3) {
			const hasPair = handCounter.some(dice => dice === 2);
			if (hasPair) {
				return HandCombinations.FULL_HOUSE;
			}
			else {
				return HandCombinations.THREE_KIND;
			}
		}

		// check pairs
		let pairSum = handCounter.filter(dice => dice === 2).length;

		if (pairSum === 2) {
			return HandCombinations.TWO_PAIRS;
		}
		else if (pairSum === 1) {
			return HandCombinations.PAIR;
		}
		else {
			return HandCombinations.NONE;
		}
	}

	evaluateHands(firstDices = [], secondDices = []) {
		const firstCombination = this.getHandCombination(firstDices);
		const secondCombination = this.getHandCombination(secondDices);

		if (firstCombination > secondCombination) {
			return GameState.FIRST_WIN;
		}
		else if (secondCombination > firstCombination) {
			return GameState.SECOND_WIN;
		}
		else {
			// compare values
			if (firstCombination === HandCombinations.NONE) {
				return GameState.TIE;
			}

			// get max and compare zipped results
			const firstCombinationList = this._getDiceSideByOccurrence(
					this._getHandCounter(firstDices),
				),
				secondCombinationList = this._getDiceSideByOccurrence(
					this._getHandCounter(secondDices),
				);

			const dices = Object.keys(firstCombinationList).sort().reverse(); // 5 ... 0

			for (const dice of dices) {
				const pairedLists = zip(firstCombinationList[dice], secondCombinationList[dice]); // (5, 3), (2, 1), ...
				for (const pair of pairedLists) {
					if (pair[0] > pair[1]) {
						return GameState.FIRST_WIN;
					}
					else if (pair[0] < pair[1]) {
						return GameState.SECOND_WIN;
					}
				}
			}

			return GameState.TIE;

		}
	}
}

export default new DiceManager();

export class PokerTextPresentation {
	constructor({separator = '   ', dices = [
		[':one:', ':two:', ':three:', ':four:', ':five:', ':six:']
	]} = {}) {
		this.dices = dices
		this.separator = separator
	}

	getDiceRepresentation(dices = []) {
		/* Get text representation of given dices */
		const result = [];
		for (const dice of dices) {
			result.push(this.dices[dice - 1]);
		}

		return result.join(this.separator).trim();
	}
}

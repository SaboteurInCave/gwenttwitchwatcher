export const factionsEnum = Object.freeze({
	NR: 'NR',
	NG: 'NG',
	SC: 'SC', // Scoatael
	MO: 'MO',
	SK: 'SK',
	NT: 'NT',
	SY: 'SY',
	NONE: 'NONE',
});

export const argsEnum = Object.freeze({
	NONE: 'NONE',
	REQUIRED: 'REQUIRED',
	OPTIONAL: 'OPTIONAL',
});

export const profileMode = Object.freeze({
	REGULAR: 'Рейтинговая таблица',
	PRO: 'Pro Rank',
});

export const channelThemes = Object.freeze({
	INFORMATION: 'information',
    TOURNAMENTS: 'tournaments',
	MEMES: 'memes',
	LORE: 'lore',
	CONTENT: 'content'
});

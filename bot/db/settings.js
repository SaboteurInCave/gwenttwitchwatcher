const mongoose = require('mongoose');

export default mongoose.model('Settings', new mongoose.Schema({
	// if player will have more MMR than listed in variable, he will have top player role
	topPlayerLimit: {
		type: Number,
		default: 9500
	},
	// minimum amount of wins for getting the specific faction role
	minFactionRoleLimit: {
		type: Number,
		default: 500
	},
	// total amount of top player role
	topPlayerAmount: {
		type: Number,
		default: 5
	}
}));

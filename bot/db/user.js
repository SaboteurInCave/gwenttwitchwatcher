import { factionsEnum } from '../enums';

const mongoose = require('mongoose');

export default mongoose.model('User', new mongoose.Schema({
	// stream data
	id: String,
	gog: { type: String, index: true},
	hidden: {type: Boolean, default: false},
	permission: {type: Boolean, default: true},
	factionChoice: {
		faction: {type: String, default: factionsEnum.NONE},
		timestamp: {type: Number, default: 0}
	}
}));

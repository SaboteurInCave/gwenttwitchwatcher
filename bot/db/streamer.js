const mongoose = require('mongoose');

export default mongoose.model('Streamer', new mongoose.Schema({
	// stream data
	id: Number,
	/*
	broadcaster_language: String,
	name: String,
	display_name: String,
	preview: String,
	views: Number,
	logo: Number,
	created_at: String,
	*/
	// message id in the channel
	message: String,
}));

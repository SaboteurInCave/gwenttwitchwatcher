import { factionsEnum } from '../enums';
import { removeSpaces } from '../utils';

const mongoose = require('mongoose');

export default mongoose.model('Stats', new mongoose.Schema({
	player: {type: String, index: true},
	timestamp: {type: Number, index: true},
	profile: {
		name: String,
		level: Number,
		prestige: Number,
		rank: Number,
		mode: String,
		position: Number,
		mmr: Number,
		avatar: Number,
		border: Number
	},
	wins: {
		total: {
			overall: Number,
			[factionsEnum.NR]: Number,
			[factionsEnum.NG]: Number,
			[factionsEnum.SC]: Number,
			[factionsEnum.MO]: Number,
			[factionsEnum.SK]: Number,
			[factionsEnum.SY]: Number,
		},
		season: {
			overall: Number,
			[factionsEnum.NR]: Number,
			[factionsEnum.NG]: Number,
			[factionsEnum.SC]: Number,
			[factionsEnum.MO]: Number,
			[factionsEnum.SK]: Number,
			[factionsEnum.SY]: Number,
		},
	},
	season: {
		overall: { mmr: Number, games: Number },
		games: { wins: Number, loses: Number, ties: Number, winrate: Number },
		factions: {
			[factionsEnum.NR]: { mmr: Number, games: Number },
			[factionsEnum.NG]: { mmr: Number, games: Number },
			[factionsEnum.SC]: { mmr: Number, games: Number },
			[factionsEnum.MO]: { mmr: Number, games: Number },
			[factionsEnum.SK]: { mmr: Number, games: Number },
			[factionsEnum.SY]: { mmr: Number, games: Number },
		},
	},
}));

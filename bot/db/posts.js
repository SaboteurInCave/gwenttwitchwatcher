const mongoose = require('mongoose');

export default mongoose.model('Posts', new mongoose.Schema({
	type: String,
	latest: Number,
	posts: [{
		post_id: {type: Number, index: true},
		timestamp: Number
	}]
}));
